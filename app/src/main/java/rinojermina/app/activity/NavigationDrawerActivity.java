package rinojermina.app.activity;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import rinojermina.app.R;
import rinojermina.app.fragments.MessageHistoryFragment;
import rinojermina.app.model.MenuItem;
import rinojermina.app.utils.SystemUtil;

public class NavigationDrawerActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private TextView fragmentTitle;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        fragmentTitle = findViewById(R.id.fragmentTitle);
        setupToolBar();
    }

    private void setupToolBar() {
        toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, rinojermina.app.R.string.drawer_open, rinojermina.app.R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(view -> drawerLayout.openDrawer(GravityCompat.START));
//        setBurger();
//        toggle.setHomeAsUpIndicator(R.drawable.ic_burger_menu);
        drawerLayout.addDrawerListener(toggle);
        drawerLayout.post(toggle::syncState);
//        onMenuItemSelected(0);
    }

    public void setBurger() {
        if (SystemUtil.isReaded(this)) {
            toggle.setHomeAsUpIndicator(R.drawable.ic_burger_pink_point);
        } else {
            toggle.setHomeAsUpIndicator(R.drawable.ic_burger_menu);
        }

    }

//    public void setReadedBurger() {
//        toggle.setHomeAsUpIndicator(R.drawable.ic_burger_menu);
//    }

    public void close() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawers();
        }
    }

    public void setTitle(String title) {
        if (getSupportActionBar() != null) {
            fragmentTitle.setText(title);
//            getSupportActionBar().setTitle(title);
//            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
    }
}