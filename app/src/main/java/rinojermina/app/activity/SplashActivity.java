package rinojermina.app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import rinojermina.app.R;
import rinojermina.app.utils.SystemUtil;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        SystemUtil.read(this, false);
        Intent intent;
        if (SystemUtil.isLogin(this)){
            intent = new Intent(this, NavigationDrawerActivity.class);
        } else {
            intent = new Intent(this, MainActivity.class);
        }
        startActivity(intent);
        finish();
    }
}