package rinojermina.app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.widget.TextView;

import rinojermina.app.R;
import rinojermina.app.utils.OurViewClient;

public class WebActivity extends AppCompatActivity {
    public static final String LINK = "link";
    public static final String TITLE = "title";

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        webView = findViewById(R.id.webView);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        ImageView help = toolbar.findViewById(R.id.help);
//        help.setVisibility(View.GONE);
//        toolbar.setNavigationOnClickListener(this);

        String link = getIntent().getStringExtra(LINK);
        String title = getIntent().getStringExtra(TITLE);
//        if (!TextUtils.isEmpty(title)) {
//            TextView textView = toolbar.findViewById(R.id.toolbar_title);
//            textView.setText(title);
//        }
//        webView.setWebViewClient(new OurViewClient());
        webView.setWebViewClient(new OurViewClient(){
            public void onPageFinished(WebView webView, String link) {
                CookieSyncManager.getInstance().sync();
            }
        });
        if (!TextUtils.isEmpty(link)) {
            loadPage(link);
        }
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setHomeButtonEnabled(true);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_menu));
//
//        }
        webView.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void loadPage(String link) {
        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.loadUrl(link);
    }
}