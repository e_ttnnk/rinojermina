package rinojermina.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import rinojermina.app.R;
import rinojermina.app.api.response.all_reminders.Task;
import rinojermina.app.view.listener.TaskClickListener;
import rinojermina.app.view.listener.TaskDeleteClickListener;

public class AddOtherDrug1Adapter extends RecyclerView.Adapter<AddOtherDrug1Adapter.ViewHolder> {

    private Context context;
    private List<Task> tasks;
    private TaskDeleteClickListener listener;

    public AddOtherDrug1Adapter(Context context, List<Task> tasks, TaskDeleteClickListener listener) {
        this.context = context;
        this.tasks = tasks;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AddOtherDrug1Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.add_reminder_1_row, viewGroup, false);
        return new AddOtherDrug1Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AddOtherDrug1Adapter.ViewHolder viewHolder, int i) {
        Task task = tasks.get(i);
        viewHolder.remText.setText(task.getName());
        viewHolder.trash.setImageResource(R.drawable.ic_trash);
        viewHolder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tasks.remove(task);
                notifyItemRemoved(i);
                notifyDataSetChanged();
                listener.onDeleteTaskClicked(task);
            }
        });
        viewHolder.reminderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onShowTaskClicked(task);
                notifyItemChanged(i, tasks.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayoutCompat reminderLayout;
        AppCompatTextView remText;
        AppCompatImageView trash;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            reminderLayout = itemView.findViewById(R.id.reminderLayout);
            remText = itemView.findViewById(R.id.remText);
            trash = itemView.findViewById(R.id.trash);
        }
    }

    }
