package rinojermina.app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import rinojermina.app.R;
import rinojermina.app.model.MenuItem;
import rinojermina.app.utils.MenuUtil;
import rinojermina.app.view.listener.MenuSelectedListener;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private Context context;
    private List<MenuItem> menuItems;
    private int selectedItem;
    private MenuSelectedListener listener;

    public MenuAdapter (Context context, MenuSelectedListener listener) {
        this.context = context;
        selectedItem = 0;
        this.listener = listener;
        menuItems = MenuUtil.generateMenu(context);
    }

    @NonNull
    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_row, viewGroup, false);
        return new MenuAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {
        viewHolder.icon.setImageResource(menuItems.get(i).getIcon());
        viewHolder.text.setText(menuItems.get(i).getText());
        if (i == selectedItem) {
            viewHolder.text.setTextColor(
                    context.getResources().getColor(R.color.menu_selected_item_background));
            viewHolder.icon.setColorFilter(
                    context.getResources().getColor(R.color.menu_selected_item_background));
//            viewHolder.mark.setVisibility(View.VISIBLE);
        } else {
            viewHolder.text.setTextColor(
                    context.getResources().getColor(R.color.menu_item));
            viewHolder.icon.setColorFilter(
                    context.getResources().getColor(R.color.menu_item));
//            viewHolder.mark.setVisibility(View.GONE);
        }
        viewHolder.layout.setOnClickListener(v -> {
            onItemSelected(i);
        });
    }

    private void onItemSelected(int i) {
        selectedItem = i;
        listener.onMenuItemSelected(i);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layout;
        private AppCompatImageView icon;
        private AppCompatTextView text;
//        private AppCompatImageView mark;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.menuRowLayout);
            icon = itemView.findViewById(R.id.menuIcon);
            text = itemView.findViewById(R.id.menuText);
//            mark = itemView.findViewById(R.id.menuSelectedMark);
        }
    }
}
