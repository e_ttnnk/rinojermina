package rinojermina.app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import rinojermina.app.R;
import rinojermina.app.api.response.Push;
import rinojermina.app.model.MenuItem;
import rinojermina.app.view.listener.PushClickListener;

public class MessageHistoryAdapter extends RecyclerView.Adapter<MessageHistoryAdapter.ViewHolder> {

    private Context context;
    private List<Push> pushes;
    private PushClickListener listener;

    public MessageHistoryAdapter(Context context, List<Push> pushes, PushClickListener listener) {
        this.context = context;
        this.pushes = pushes;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MessageHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.message_row, viewGroup, false);
        return new MessageHistoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageHistoryAdapter.ViewHolder viewHolder, int i) {
        Push push = pushes.get(i);
        String readed = push.getReaded();
        if (readed.equals("0")) {
            viewHolder.icon.setImageResource(R.drawable.ic_pink_msg_history_clndr);
            viewHolder.date.setTextColor(
                    context.getResources().getColor(R.color.menu_selected_item_background));
            viewHolder.mark.setImageResource(R.drawable.ic_pink_point);
            // TODO: 28.01.2022 mark установить
        } else {
            viewHolder.icon.setImageResource(R.drawable.ic_grey_msg_history_clndr);
            viewHolder.date.setTextColor(
                    context.getResources().getColor(R.color.light_grey));
        }
        viewHolder.date.setText(push.getDate());
        viewHolder.text.setText(push.getText());
        viewHolder.msgLayout.setOnClickListener(view -> {
            listener.onMessageClicked(push);
            push.setReaded("1");
            viewHolder.icon.setImageResource(R.drawable.ic_grey_msg_history_clndr);
            viewHolder.date.setTextColor(
                    context.getResources().getColor(R.color.light_grey));
            notifyItemChanged(i);
        });
    }


    @Override
    public int getItemCount() {
        return pushes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayoutCompat msgLayout;
        AppCompatImageView icon;
        AppCompatTextView date;
        AppCompatImageView mark;
        AppCompatTextView text;

        ViewHolder(View itemView) {
            super(itemView);
            msgLayout = itemView.findViewById(R.id.msgLayout);
            icon = itemView.findViewById(R.id.icon);
            date = itemView.findViewById(R.id.date);
            mark = itemView.findViewById(R.id.mark);
            text = itemView.findViewById(R.id.text);
        }
    }
}
