package rinojermina.app.api;

import com.google.gson.JsonObject;

import java.util.ArrayList;

public class JsonFactory {
    public static final String ID = "id";
    private static final String USER = "user";
    public static final String PHONE = "phone";
    public static final String TOKEN = "token";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String BIRTH_YEAR = "birth_year";
//    public static final String BIRTH_YEAR_PASS_UPD = "birth_year";
    public static final String PASSWORD = "password";
    public static final String DOC_FIO = "doc_fio";
    public static final String DOC_PHONE = "doc_phone";
    public static final String REG_DATE = "regDate";

    public static final String TASK = "task";
    public static final String TASK_ID = "id";
    public static final String STAGE = "stage";
//    public static final Integer STAGE = "stage";
    public static final String DATE_BEGIN = "date_begin";
    public static final String DATE_END = "date_end";
    public static final String DAY = "day";
    public static final String NIGHT = "night";

    public static final String PUSH = "push";
    public static final String PUSH_ID = "push_id";

    public static final String NAME = "name";
    public static final String TIMES = "times";


    public static JsonObject calendarBody (String token, Integer stage, String  date_begin,
                                           String day, String night) {
        JsonObject userObject = new JsonObject();
        userObject.addProperty(TOKEN, token);
        JsonObject taskObject = new JsonObject();
//        taskObject.addProperty(String.valueOf(STAGE), stage);
        taskObject.addProperty(STAGE, stage);
        taskObject.addProperty(DATE_BEGIN, date_begin);
        taskObject.addProperty(DAY, day);
        taskObject.addProperty(NIGHT, night);
        JsonObject body = new JsonObject();
        body.add(USER, userObject);
        body.add(TASK, taskObject);
        return body;
    }

    public static JsonObject loginBody (String login, String pass){
        JsonObject innerObject = new JsonObject();
        innerObject.addProperty(PHONE, login.trim());
        innerObject.addProperty(PASSWORD, pass);
        JsonObject body = new JsonObject();
        body.add(USER, innerObject);
        return body;
    }

    public static JsonObject regBody (String phone, String pass, String birthYear,
                                      String doc_fio, String doc_phone) {
        JsonObject innerObject = new JsonObject();
//        innerObject.addProperty(TOKEN, token);
        innerObject.addProperty(PHONE, phone.trim());
        innerObject.addProperty(PASSWORD, pass);
        innerObject.addProperty(BIRTH_YEAR, birthYear);
        innerObject.addProperty(DOC_FIO, doc_fio);
        innerObject.addProperty(DOC_PHONE, doc_phone);
        JsonObject body = new JsonObject();
        body.add(USER, innerObject);
        return body;
    }

    public static JsonObject fcmTokenUpd(String token, String fcm_token) {
        JsonObject innerObject = new JsonObject();
        innerObject.addProperty(TOKEN, token);
        innerObject.addProperty(FCM_TOKEN, fcm_token);
        JsonObject body = new JsonObject();
        body.add(USER, innerObject);
        return body;
    }

    public static JsonObject passUpdStep1Body (String phone, String birthYear) {
        JsonObject innerObject = new JsonObject();
        innerObject.addProperty(PHONE, phone.trim());
        innerObject.addProperty(BIRTH_YEAR, birthYear);
        JsonObject body = new JsonObject();
        body.add(USER, innerObject);
        return body;
    }

    public static JsonObject passUpdStep2Body (String token, String pass) {
        JsonObject innerObject = new JsonObject();
        innerObject.addProperty(TOKEN, token);
        innerObject.addProperty(PASSWORD, pass);
        JsonObject body = new JsonObject();
        body.add(USER, innerObject);
        return body;
    }

    public static JsonObject tokenObject(String token) {
        JsonObject innerObject = new JsonObject();
        innerObject.addProperty(TOKEN, token);
        JsonObject body = new JsonObject();
        body.add(USER, innerObject);
        return body;
    }

    public static JsonObject createReminderAndroid(String token, String name, ArrayList<String> times){
        JsonObject userObject = new JsonObject();
        userObject.addProperty(TOKEN, token);
        JsonObject body = new JsonObject();
        body.add(USER, userObject);

        JsonObject taskObject = new JsonObject();
        taskObject.addProperty(NAME, name);

        JsonObject timesObject = new JsonObject();
        for (int i = 0; i < times.size(); i++){
            String time = times.get(i);
            String key = "Текст пуш" + (i + 1);
            timesObject.addProperty(key, time.toString());
        }
        taskObject.add(TIMES, timesObject);
        body.add(TASK, taskObject);

        return body;
    }

    public static JsonObject delOneReminderBody(String token, String id) {
        JsonObject userObject = new JsonObject();
        userObject.addProperty(TOKEN, token);
        JsonObject taskObject = new JsonObject();
        taskObject.addProperty(ID, id);
        JsonObject body = new JsonObject();
        body.add(USER, userObject);
        body.add(TASK, taskObject);
        return body;
    }

    public static JsonObject delOnePushBody(String token, String id) {
        JsonObject userObject = new JsonObject();
        userObject.addProperty(TOKEN, token);
        JsonObject taskObject = new JsonObject();
        taskObject.addProperty(ID, id);
        JsonObject body = new JsonObject();
        body.add(USER, userObject);
        body.add(TASK, taskObject);
        return body;
    }

    public static JsonObject updatePushStatusBody(String token, String push_id) {
        JsonObject userObject = new JsonObject();
        userObject.addProperty(TOKEN, token);
        userObject.addProperty(PUSH_ID, push_id);
        JsonObject body = new JsonObject();
        body.add(USER, userObject);
        return body;
    }

}
