package rinojermina.app.api;

import com.google.gson.JsonObject;

import java.security.PublicKey;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rinojermina.app.api.response.CalendarResponse;
import rinojermina.app.api.response.ChangePassStep1Response;
import rinojermina.app.api.response.ChangePassStep2Response;
import rinojermina.app.api.response.LoginResponse;
import rinojermina.app.api.response.ProfileCreateResponse;
import rinojermina.app.api.response.PushHistoryResponse;
import rinojermina.app.api.response.PushStatusResponse;
import rinojermina.app.api.response.all_reminders.AllRemindersResponse;
import rinojermina.app.api.response.new_reminder.NewReminderResponse;
import rinojermina.app.api.response.one_push_del.OnePushDelResponse;

public interface RinoApi {
//    @FormUrlEncoded
//    @POST("auth")
//    Observable<StringResponse> auth(@Field("phone") String phone,
//                                    @Field("password") String password);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("auth")
    Observable<LoginResponse> auth(@Body JsonObject body);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("registration")
    Observable<ProfileCreateResponse> registration(@Body JsonObject body);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("change_pass_step_1")
    Observable<ChangePassStep1Response> change_pass_step_1(@Body JsonObject body);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("change_pass_step_2")
    Observable<ChangePassStep2Response> change_pass_step_2(@Body JsonObject body);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("calendar")
    Observable<CalendarResponse> calendar(@Body JsonObject body);

//    @Headers({"Content-type: application/json",
//            "Accept: application/json"})
//    @POST("update_fcm_token")
//    Observable<ProfileCreateResponse> update_fcm_token(@Body JsonObject body);

    @GET("calendar")
    Observable<CalendarResponse> getCalendar(@Query("token") String token);

//    @GET("push_history")
//    Observable<PushHistoryResponse> getPushHistory(@Query("token") String token);

    @GET("push_history_android")
    Observable<PushHistoryResponse> getPushHistory(@Query("token") String token);

    @GET("get_one_push")
    Observable<PushHistoryResponse> getOnePush(@Query("token") String token,
                                               @Query("push_id") String pushId);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("update_push")
    Observable<PushStatusResponse> updatePush(@Body JsonObject body);

    @GET("get_reminders")
    Observable<AllRemindersResponse> getReminders(@Query("token") String token);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("create_reminder_android")
    Observable<NewReminderResponse> newReminder(@Body JsonObject body);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("remove_reminder")
    Observable<OnePushDelResponse> delOneReminder(@Body JsonObject body);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("remove_one_remind")
    Observable<OnePushDelResponse> delOnePush(@Body JsonObject body);

    @GET("get_one_reminder")
    Observable<AllRemindersResponse> getOneReminder(@Query("token") String token,
                                                    @Query("task_id") String id);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("update_fcm_token")
    Observable<LoginResponse> updateFcmTokenAuth(@Body JsonObject body);

    @Headers({"Content-type: application/json",
            "Accept: application/json"})
    @POST("update_fcm_token")
    Observable<ProfileCreateResponse> updateFcmTokenReg(@Body JsonObject body);

}
