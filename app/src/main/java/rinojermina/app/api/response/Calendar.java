package rinojermina.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Calendar {
    @SerializedName("date_begin")
    @Expose
    private String dateBegin;
    @SerializedName("date_end")
    @Expose
    private String dateEnd;

    public String getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public ArrayList<Calendar> getRange() {
        ArrayList<Calendar> rangeList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM hh:mm:ss");
        java.util.Calendar calendarBegin = java.util.Calendar.getInstance();
        long daysDiff = 0;
        try {
            Date dateBegin = dateFormat.parse(getDateBegin());
            calendarBegin = java.util.Calendar.getInstance();
            calendarBegin.setTime(dateBegin);

            Date dateEnd = dateFormat.parse(getDateEnd());
            java.util.Calendar calendarEnd = java.util.Calendar.getInstance();
            calendarEnd.setTime(dateEnd);

            long msDiff = calendarEnd.getTimeInMillis() - calendarBegin.getTimeInMillis();
            daysDiff = TimeUnit.MILLISECONDS.toDays(msDiff);

        } catch (Exception e) {
            e.printStackTrace();
        }

//        Calendar firsDay = new Calendar();
//        firsDay.dateBegin = dateBegin;
//        firsDay.dateEnd = dateEnd;
//
//        rangeList.add(firsDay);
        for (int i = 0; i < daysDiff; i++) {
            java.util.Calendar startCal = java.util.Calendar.getInstance();
            startCal.setTimeInMillis(calendarBegin.getTimeInMillis());

            startCal.add(java.util.Calendar.DAY_OF_MONTH, i);

            Calendar curDay = new Calendar();
            curDay.dateBegin = dateFormat.format(startCal.getTime());
            curDay.dateEnd = dateEnd;

            rangeList.add(curDay);
        }

        return rangeList;
    }

    public java.util.Calendar getCalendarBegin () {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM hh:mm:ss");
            java.util.Calendar calendarBegin = java.util.Calendar.getInstance();
            Date dateBegin = dateFormat.parse(getDateBegin());
            calendarBegin = java.util.Calendar.getInstance();
            calendarBegin.setTime(dateBegin);
            return calendarBegin;
        } catch (Exception edf) {
            return null;
        }
    }
    public Date getDateDateBegin() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            return dateFormat.parse(dateBegin);
        } catch (Exception e) {
            return null;
        }
    }

    public Calendar() {

    }

    public Calendar(long timeInMillis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTimeInMillis(timeInMillis);
        Calendar calendar = new Calendar();
        calendar.dateBegin = dateFormat.format(cal.getTime());
        calendar.dateEnd = dateFormat.format(cal.getTime());
    }
}
