package rinojermina.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarResponse {
//    @SerializedName("date_begin")
//    @Expose
//    private String dateBegin;
//    @SerializedName("date_end")
//    @Expose
//    private String dateEnd;

    @SerializedName("calendar")
    @Expose
//    private List<Calendar> calendar = null;
    private ArrayList<rinojermina.app.api.response.Calendar> calendar = null;

//    public String getDateBegin() {
//        return dateBegin;
//    }
//
//    public void setDateBegin(String dateBegin) {
//        this.dateBegin = dateBegin;
//    }
//
//    public String getDateEnd() {
//        return dateEnd;
//    }
//
//    public void setDateEnd(String dateEnd) {
//        this.dateEnd = dateEnd;
//    }

//    public List<Calendar> getCalendar() {
//        return calendar;
//    }
    public ArrayList<rinojermina.app.api.response.Calendar> getCalendar() {
        return calendar;
    }

//    public void setCalendar(List<Calendar> calendar) {
//        this.calendar = calendar;
//    }
    public void setCalendar(ArrayList<rinojermina.app.api.response.Calendar> calendar) {
        this.calendar = calendar;
    }
}
