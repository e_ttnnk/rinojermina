package rinojermina.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Push {
    @SerializedName("push_id")
    @Expose
    private String pushId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("readed")
    @Expose
    private String readed;
    @SerializedName("text")
    @Expose
    private String text;

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReaded() {
        return readed;
    }

    public void setReaded(String readed) {
        this.readed = readed;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
