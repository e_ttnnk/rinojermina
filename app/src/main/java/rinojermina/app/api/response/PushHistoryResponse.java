package rinojermina.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PushHistoryResponse {
    @SerializedName("push")
    @Expose
    private List<Push> push = null;

    public List<Push> getPush() {
        return push;
    }

    public void setPush(List<Push> push) {
        this.push = push;
    }
}
