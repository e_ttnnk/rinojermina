package rinojermina.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StringResponse {
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    @SerializedName("processed")
    @Expose
    private Boolean processed;
    @SerializedName("data")
    @Expose
    private String data;

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
