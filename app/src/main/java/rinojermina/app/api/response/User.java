package rinojermina.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static rinojermina.app.api.JsonFactory.*;

public class User {
    @SerializedName(ID)
    @Expose
    private String id;
    @SerializedName(PHONE)
    @Expose
    private String phone;
    @SerializedName(TOKEN)
    @Expose
    private String token;
    @SerializedName(FCM_TOKEN)
    @Expose
    private String fcm_token;
    @SerializedName(PASSWORD)
    @Expose
    private String password;
    @SerializedName(BIRTH_YEAR)
    @Expose
    private String birth_year;
//    @SerializedName(BIRTH_YEAR_PASS_UPD)
//    @Expose
//    private String birth_year;
    @SerializedName(DOC_FIO)
    @Expose
    private String doc_fio;
    @SerializedName(DOC_PHONE)
    @Expose
    private String doc_phone;
    @SerializedName(REG_DATE)
    @Expose
    private String regDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String name) {
        this.password = name;
    }

    public String getBirth_year() {
        return birth_year;
    }

    public void setBirth_year(String birth_year) {
        this.birth_year = birth_year;
    }

    public String getBirthYearPassUpd() {
        return birth_year;
    }

    public void setBirthYearPassUpd(String birthYear) {
        this.birth_year = birthYear;
    }

    public String getDoc_fio() { return doc_fio; }

    public void setDoc_fio(String doc_fio) { this.doc_fio = doc_fio; }

    public String getDoc_phone() { return doc_phone; }

    public void setDoc_phone(String doc_phone) { this.doc_phone = doc_phone; }

    public String getRegDate() { return regDate; }

    public void setRegDate(String regDate) { this.regDate = regDate; }


}
