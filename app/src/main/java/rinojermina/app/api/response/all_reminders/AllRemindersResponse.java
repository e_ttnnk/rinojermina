package rinojermina.app.api.response.all_reminders;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllRemindersResponse {
    @SerializedName("task")
    @Expose
    private List<Task> task = null;

    public List<Task> getTask() {
        return task;
    }

    public void setTask(List<Task> task) {
        this.task = task;
    }
}
