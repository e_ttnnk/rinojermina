package rinojermina.app.api.response.new_reminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewReminderResponse {
    @SerializedName("task")
    @Expose
    private Task task;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
