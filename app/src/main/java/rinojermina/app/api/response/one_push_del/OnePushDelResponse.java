package rinojermina.app.api.response.one_push_del;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnePushDelResponse {
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("task")
    @Expose
    private Task task;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
