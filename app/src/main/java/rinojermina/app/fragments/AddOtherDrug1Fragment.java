package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.adapter.AddOtherDrug1Adapter;
import rinojermina.app.adapter.MessageHistoryAdapter;
import rinojermina.app.api.response.all_reminders.Task;
import rinojermina.app.presenter.AddDrug1Presenter;
import rinojermina.app.presenter.MessageHistoryPresenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IAddDrug1View;
import rinojermina.app.view.listener.TaskDeleteClickListener;

import static rinojermina.app.activity.WebActivity.TITLE;

public class AddOtherDrug1Fragment extends BaseFragment<AddDrug1Presenter> implements IAddDrug1View,
        TaskDeleteClickListener {

    private String title;
    RecyclerView drugReminderRecycler;
    LinearLayoutCompat addReminderLayout;

    private AddDrug1Presenter presenter;

    public AddOtherDrug1Fragment() {
        // Required empty public constructor
    }

    public static AddOtherDrug1Fragment newInstance(String title) {
        AddOtherDrug1Fragment fragment = new AddOtherDrug1Fragment();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    public static AddOtherDrug1Fragment newInstance() {
        AddOtherDrug1Fragment fragment = new AddOtherDrug1Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.title = getArguments().getString(TITLE);
        }
        presenter = new AddDrug1Presenter(this);
        setPresenter(presenter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_other_drug1, container, false);
        setTitle(title);
        drugReminderRecycler = rootView.findViewById(R.id.drugReminderRecycler);
        addReminderLayout = rootView.findViewById(R.id.addReminderLayout);
        addReminderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                replaceFragment(AddOtherDrug2Fragment.newInstance());
                replaceFragment(AddOtherDrug2Fragment.newInstance());
            }
        });
        presenter.remindersShow(SystemUtil.getToken(getContext()));
//        presenter.remindersShow("plZVF1RdbipPRb9VobGS4in7R8Rf5sClEUdlESEmLf5JeN5G3R");
        return rootView;
    }

    public void setTitle(String title) {
        if (title != null && getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setTitle(title);
        }
    }

    @Override
    public void showReminders(List<Task> tasks) {
        drugReminderRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        drugReminderRecycler.setAdapter(new AddOtherDrug1Adapter(getContext(), tasks, this));
    }

    @Override
    public void delReminder(rinojermina.app.api.response.one_push_del.Task task) {
        Log.d("WTF", "WTF");
    }

    @Override
    public void showOneReminder(List<Task> tasks) {
    }

    @Override
    public void showError(Throwable error) {

    }

    @Override
    public void onDeleteTaskClicked(Task task) {
        presenter.delOneReminder(SystemUtil.getToken(getContext()), task.getId());
    }

    @Override
    public void onShowTaskClicked(Task task) {
        SystemUtil.setTaskId(getContext(), task.getId());
        replaceFragment(OneReminderFragment.newInstance());

//        presenter.oneReminderShow(SystemUtil.getToken(getContext()),task.getId());
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager manager = requireActivity().getSupportFragmentManager();
        if (manager != null) {
            manager.beginTransaction()
                    .replace(rinojermina.app.R.id.container, fragment).addToBackStack(null).commit();
        }
    }
}