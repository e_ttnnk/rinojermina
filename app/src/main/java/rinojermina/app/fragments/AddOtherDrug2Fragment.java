package rinojermina.app.fragments;

import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Objects;

import rinojermina.app.R;
import rinojermina.app.api.response.one_push_del.Task;
import rinojermina.app.presenter.AddDrug2Presenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IAddDrug2View;

public class AddOtherDrug2Fragment extends BaseFragment<AddDrug2Presenter> implements IAddDrug2View {
    LinearLayoutCompat reminder1Layout;
    LinearLayoutCompat reminder2Layout;
    LinearLayoutCompat reminder3Layout;
    AppCompatEditText reminderName;
    AppCompatTextView reminderTime1;
    AppCompatImageView trash1;
    AppCompatTextView reminderTime2;
    AppCompatImageView trash2;
    AppCompatTextView reminderTime3;
    AppCompatImageView trash3;
    LinearLayoutCompat addReminderLayout;
    AppCompatButton saveReminderBtn;

    Calendar setTime = Calendar.getInstance();

    private AddDrug2Presenter presenter;

    public AddOtherDrug2Fragment() {
        // Required empty public constructor
    }

    public static AddOtherDrug2Fragment newInstance() {
        AddOtherDrug2Fragment fragment = new AddOtherDrug2Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
        presenter = new AddDrug2Presenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_other_drug2, container, false);

        reminder1Layout = rootView.findViewById(R.id.reminder1Layout);
        reminder2Layout = rootView.findViewById(R.id.reminder2Layout);
        reminder3Layout = rootView.findViewById(R.id.reminder3Layout);
        reminderName = rootView.findViewById(R.id.reminderName);
        reminderTime1 = rootView.findViewById(R.id.reminderTime1);
        reminderTime1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 02.02.2022 по аналогии в остальных сделать
                setReminderTime1();
            }
        });

        trash1 = rootView.findViewById(R.id.trash1);
        trash1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reminder1Layout.setVisibility(View.GONE);
//                presenter.delOnePushReminder(SystemUtil.getToken(getContext()),
//                        presenter.time1Id);
                presenter.time1 = AddDrug2Presenter.NOT_SET;
                refreshVisibility();
            }
        });

        reminderTime2 = rootView.findViewById(R.id.reminderTime2);
        reminderTime2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setReminderTime2();
            }
        });

        trash2 = rootView.findViewById(R.id.trash2);
        trash2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reminder2Layout.setVisibility(View.GONE);

//                presenter.delOnePushReminder(SystemUtil.getToken(getContext()),
//                        presenter.time2Id);
                presenter.time2 = AddDrug2Presenter.NOT_SET;
                refreshVisibility();
            }
        });

        reminderTime3 = rootView.findViewById(R.id.reminderTime3);
        reminderTime3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setReminderTime3();
            }
        });

        trash3 = rootView.findViewById(R.id.trash3);
        trash3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reminder3Layout.setVisibility(View.GONE);
//                presenter.delOnePushReminder(SystemUtil.getToken(getContext()),
//                        presenter.time3Id);
                presenter.time3 = AddDrug2Presenter.NOT_SET;
                refreshVisibility();
            }
        });
        addReminderLayout = rootView.findViewById(R.id.addReminderLayout);
        addReminderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reminder2Layout.getVisibility() == View.GONE &&
                        reminder3Layout.getVisibility() == View.GONE) {
                    presenter.time2 = "8:00";
                    reminder2Layout.setVisibility(View.VISIBLE);
                } else {
                    presenter.time3 = "8:00";
                    reminder3Layout.setVisibility(View.VISIBLE);
                    addReminderLayout.setVisibility(View.GONE);
                }
                refreshVisibility();
            }
        });
        saveReminderBtn = rootView.findViewById(R.id.saveReminderBtn);
        saveReminderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.reminderCreate(SystemUtil.getToken(getContext()),
                        Objects.requireNonNull(reminderName.getText()).toString());
            }
        });
        presenter.time1 = "8:00";
        refreshVisibility();
        return rootView;
    }

    //
    @Override
    public void reminderCreate() {
        // TODO: 04.02.2022 что тут отобразить ?
        replaceFragment(AddOtherDrug1Fragment.newInstance());
    }

    @Override
    public void delOnePush(Task task) {
//        reminder2Layout.setVisibility(View.GONE);
        // TODO: 04.02.2022 что тут делать\отображать\сохранять?
    }

    @Override
    public void showError(Throwable error) {

    }

    // TODO: 02.02.2022 остальные переделать по аналогии
    TimePickerDialog.OnTimeSetListener rt1 = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int nextMinute = 0;

            if (minute >= 45 && minute <= 59)
                nextMinute = 45;
            else if (minute >= 30)
                nextMinute = 30;
            else if (minute >= 15)
                nextMinute = 15;
            else if (minute > 0)
                nextMinute = 0;
            else {
                nextMinute = 45;
            }

            if (minute - nextMinute == 1) {
                if (minute >= 45 && minute <= 59)
                    nextMinute = 00;
                else if (minute >= 30)
                    nextMinute = 45;
                else if (minute >= 15)
                    nextMinute = 30;
                else if (minute > 0)
                    nextMinute = 15;
                else {
                    nextMinute = 15;
                }
            }
            setTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
//            setTime.set(Calendar.MINUTE, minute);
            setTime.set(Calendar.MINUTE, nextMinute);

            int hour = setTime.get(Calendar.HOUR_OF_DAY);
            int calendarMinute = setTime.get(Calendar.MINUTE);
            String timeString = String.format("%02d:%02d", hour, calendarMinute);
            presenter.time1 = timeString;
            reminderTime1.setText(timeString);
        }
    };

    private void refreshVisibility() {
        if (presenter.timesCount() > 1) {
            trash1.setVisibility(View.VISIBLE);
            trash2.setVisibility(View.VISIBLE);
            trash3.setVisibility(View.VISIBLE);
        } else {
            trash1.setVisibility(View.GONE);
            trash2.setVisibility(View.GONE);
            trash3.setVisibility(View.GONE);
        }
        if (presenter.timesCount() == 3) {
            addReminderLayout.setVisibility(View.GONE);
        } else {
            addReminderLayout.setVisibility(View.VISIBLE);
        }
    }

    public void setReminderTime1() {
        new TimePickerDialog(getContext(), rt1,
                setTime.get(Calendar.HOUR_OF_DAY),
                setTime.get(Calendar.MINUTE), true)
                .show();
    }

//    private void setInitialTime2(View v) {
//        int hour = setTime.get(Calendar.HOUR_OF_DAY);
//        int minute = setTime.get(Calendar.MINUTE);
//        String timeString = String.format("%02d:%02d", hour, minute);
//        reminderTime2.setText(timeString);
//    }

    TimePickerDialog.OnTimeSetListener rt2 = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int nextMinute = 0;

            if (minute >= 45 && minute <= 59)
                nextMinute = 45;
            else if (minute >= 30)
                nextMinute = 30;
            else if (minute >= 15)
                nextMinute = 15;
            else if (minute > 0)
                nextMinute = 0;
            else {
                nextMinute = 45;
            }

            if (minute - nextMinute == 1) {
                if (minute >= 45 && minute <= 59)
                    nextMinute = 00;
                else if (minute >= 30)
                    nextMinute = 45;
                else if (minute >= 15)
                    nextMinute = 30;
                else if (minute > 0)
                    nextMinute = 15;
                else {
                    nextMinute = 15;
                }
            }

            setTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
//            setTime.set(Calendar.MINUTE, minute);
            setTime.set(Calendar.MINUTE, nextMinute);

            int hour = setTime.get(Calendar.HOUR_OF_DAY);
            int calendarMinute = setTime.get(Calendar.MINUTE);
            String timeString = String.format("%02d:%02d", hour, calendarMinute);
            presenter.time2 = timeString;
            reminderTime2.setText(timeString);
        }
    };

    public void setReminderTime2() {
        new TimePickerDialog(getContext(), rt2,
                setTime.get(Calendar.HOUR_OF_DAY),
                setTime.get(Calendar.MINUTE), true)
                .show();
    }

//    private void setInitialTime3(View v) {
//        int hour = setTime.get(Calendar.HOUR_OF_DAY);
//        int minute = setTime.get(Calendar.MINUTE);
//        String timeString = String.format("%02d:%02d", hour, minute);
//        reminderTime3.setText(timeString);
//    }

    TimePickerDialog.OnTimeSetListener rt3 = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int nextMinute = 0;

            if (minute >= 45 && minute <= 59)
                nextMinute = 45;
            else if (minute >= 30)
                nextMinute = 30;
            else if (minute >= 15)
                nextMinute = 15;
            else if (minute > 0)
                nextMinute = 0;
            else {
                nextMinute = 45;
            }

            if (minute - nextMinute == 1) {
                if (minute >= 45 && minute <= 59)
                    nextMinute = 00;
                else if (minute >= 30)
                    nextMinute = 45;
                else if (minute >= 15)
                    nextMinute = 30;
                else if (minute > 0)
                    nextMinute = 15;
                else {
                    nextMinute = 15;
                }
            }

            setTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
//            setTime.set(Calendar.MINUTE, minute);
            setTime.set(Calendar.MINUTE, nextMinute);

            int hour = setTime.get(Calendar.HOUR_OF_DAY);
            int calendarMinute = setTime.get(Calendar.MINUTE);
            String timeString = String.format("%02d:%02d", hour, calendarMinute);
            presenter.time3 = timeString;
            reminderTime3.setText(timeString);
        }
    };

    public void setReminderTime3() {
        new TimePickerDialog(getContext(), rt3,
                setTime.get(Calendar.HOUR_OF_DAY),
                setTime.get(Calendar.MINUTE), true)
                .show();
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager manager = requireActivity().getSupportFragmentManager();
        if (manager != null) {
            manager.beginTransaction()
                    .replace(rinojermina.app.R.id.container, fragment).addToBackStack(null).commit();
        }
    }
}