package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.presenter.AppointmentCalendarPresenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IAppointmentCalendarView;

import static com.prolificinteractive.materialcalendarview.MaterialCalendarView.SELECTION_MODE_RANGE;

public class AppointmentCalendarFragment extends BaseFragment<AppointmentCalendarPresenter> implements IAppointmentCalendarView {

    MaterialCalendarView calendarView;

//    private MaterialCalendarView calendarView;
//    private ArrayList<Date> markedDates;

//    final List<String> startDates = Arrays.asList(
//            "2022-19-01 00:00:01", "2022-18-03 00:00:01"
//    );
//    final List<String> endDates = Arrays.asList(
//            "2022-26-01 23:59:59", "2022-24-03 23:59:59"
//    );

    final String DATE_FORMAT = "yyyy-dd-MM HH:mm:ss";

    private AppointmentCalendarPresenter presenter;


    public AppointmentCalendarFragment() {
        // Required empty public constructor
    }

    public static AppointmentCalendarFragment newInstance() {
        AppointmentCalendarFragment fragment = new AppointmentCalendarFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new AppointmentCalendarPresenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_appointment_calendar, container, false);
        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setTitle(getString(R.string.appointment_calendar));
        }

        calendarView = rootView.findViewById(R.id.calendarView);
//        calendarView.setSelectionMode(SELECTION_MODE_RANGE);
        calendarView.setSelectionMode(SELECTION_MODE_RANGE);
        // TODO: 03.02.2022 если не задается этот метод, взять материал календарвью,

//        calendarView.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        presenter.dateRangeShow(SystemUtil.getToken(getContext()));
//        presenter.dateRangeShow("plZVF1RdbipPRb9VobGS4in7R8Rf5sClEUdlESEmLf5JeN5G3R");

        return rootView;
    }

    @Override
    public void showDates(ArrayList<rinojermina.app.api.response.Calendar> calendars) {
//        for (int i = 0; i < calendars.size(); i++) {
//            String dateBegin = calendars.get(i).getDateBegin();
        int sdfdsfdsf = 0;
        for (int i = 0; i < calendars.size(); i++) {
            ArrayList<rinojermina.app.api.response.Calendar> range = calendars.get(i).getRange();
            for (int j = 0; j < range.size(); j++) {
                rinojermina.app.api.response.Calendar oneDay = range.get(j);
// TODO: 08.02.2022 добавить даты во вью 
                calendarView.setDateSelected(oneDay.getCalendarBegin(), false);
//                calendarView.state().edit().setMinimumDate(oneDay.getCalendarBegin()).setMaximumDate(oneDay.getDateDateBegin()).commit();
            }
        }


//
//            String dateBegin = calendars.get(0).getDateBegin();
////            String dateEnd = calendars.get(i).getDateEnd();
//            String dateEnd = calendars.get(0).getDateEnd();
////            String dateMiddle = "2022-20-01 00:00:01";
//            SimpleDateFormat f = new SimpleDateFormat(DATE_FORMAT);
//            try {
//                Date db = f.parse(dateBegin);
////                Date dm = f.parse(dateMiddle);
//                Date de = f.parse(dateEnd);
//                calendarView.setSelectedDate(db);
////                calendarView.setDateSelected(dm, true);
//                calendarView.setDateSelected(de, true);
////                        setSelectedDate(de);
////                calendarView.state().edit().setMinimumDate(db).setMaximumDate(de).commit();
////                calendarView.state().edit(). .setMinimumDate(db).setMaximumDate(de).commit();
//                //            long milliseconds = d.getTime();
////            calendarView.setSelectedDate(milliseconds, true); setDate(milliseconds);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
////        }


//        setDateToCalendar(dateBegin);
//        setDateToCalendar(dateEnd);
    }

    public void getDateDifference(LocalDate firstDate, LocalDate secondDate) {
//        Period period = Period.between(secondDate, firstDate);
//        System.out.println(period.getYears() + "." + period.getMonths() + "." + period.getDays());
    }

//    LocalDate getLocalDate(String date) {
//        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
//        try {
//            Date input = sdf.parse(date);
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(input);
//            return LocalDate.of(cal.get(Calendar.YEAR),
//                    cal.get(Calendar.MONTH) + 1,
//                    cal.get(Calendar.DAY_OF_MONTH));
//
//
//        } catch (NullPointerException e) {
//            return null;
//        } catch (ParseException e) {
//            return null;
//        }
//    }

//    public Date setDateToCalendar(String date) {
//        SimpleDateFormat f = new SimpleDateFormat(DATE_FORMAT);
//        try {
//            return f.parse(date);
////            long milliseconds = d.getTime();
////            calendarView.setSelectedDate(milliseconds, true); setDate(milliseconds);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return (date);
//    }

    @Override
    public void showError(Throwable error) {

    }
}