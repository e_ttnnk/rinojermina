package rinojermina.app.fragments;

import androidx.fragment.app.Fragment;

import rinojermina.app.presenter.BasePresenter;

public class BaseFragment<T extends BasePresenter> extends Fragment {
    private T presenter;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) getPresenter().onDestroy();
    }

    public T getPresenter() {
        return presenter;
    }

    public void setPresenter(T presenter) {
        this.presenter = presenter;
    }

//    public void setTitle(String title){
//        if(title != null && getActivity() != null && getActivity() instanceof MainActivity){
//            ((MainActivity)getActivity()).setTitle(title);
//        }
//    }
}
