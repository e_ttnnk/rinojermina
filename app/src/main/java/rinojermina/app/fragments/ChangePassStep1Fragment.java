package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import rinojermina.app.R;
import rinojermina.app.api.response.User;
import rinojermina.app.presenter.ChangePassStep1Presenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IChangePassStep1View;

public class ChangePassStep1Fragment extends BaseFragment<ChangePassStep1Presenter> implements IChangePassStep1View {

    TextInputLayout passUpdPhoneInputLayout;
    TextInputLayout passUpdBirthYearInputLayout;

    EditText phone;
    EditText birthYear;

    Button forwardBtn;
    Button backBtn;

    LinearLayoutCompat layoutBtnBack;

    private ChangePassStep1Presenter presenter;

    public ChangePassStep1Fragment() {
        // Required empty public constructor
    }

    public static ChangePassStep1Fragment newInstance() {
        ChangePassStep1Fragment fragment = new ChangePassStep1Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ChangePassStep1Presenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_update_password_forward, container, false);

        passUpdPhoneInputLayout = rootView.findViewById(R.id.passUpdPhoneInputLayout);
        passUpdBirthYearInputLayout = rootView.findViewById(R.id.passUpdBirthYearInputLayout);
        phone = rootView.findViewById(R.id.phone);
        birthYear = rootView.findViewById(R.id.birthYear);
        forwardBtn = rootView.findViewById(R.id.forwardBtn);
        backBtn = rootView.findViewById(R.id.backBtn);
        layoutBtnBack = rootView.findViewById(R.id.layoutBtnBack);

        forwardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passUpdPhoneInputLayout.setErrorEnabled(false);
                passUpdBirthYearInputLayout.setErrorEnabled(false);

                presenter.step1PassUpd(
                        phone.getText().toString(),
                        birthYear.getText().toString());

//                Navigation.findNavController(rootView).navigate(R.id.action_updatePasswordFragmentForward2_to_updatePasswordFragmentUpdate2);
            }
        });

        layoutBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(rootView).navigate(R.id.action_updatePasswordFragmentForward2_to_signInFragment2);
            }
        });

        return rootView;
    }

    @Override
    public void phoneNumberError() {
        passUpdPhoneInputLayout.setErrorEnabled(true);
        passUpdPhoneInputLayout.setError(getString(R.string.passUpdStep1Error));
    }

    @Override
    public void birthYearError() {
        passUpdBirthYearInputLayout.setErrorEnabled(true);
        passUpdBirthYearInputLayout.setError(getString(R.string.passUpdStep1Error));
    }

    @Override
    public void showNextStep(User user) {
        SystemUtil.setToken(getContext(), user.getToken());
            Navigation.findNavController(requireView()).navigate(R.id
                    .action_updatePasswordFragmentForward2_to_updatePasswordFragmentUpdate2);
    }

    @Override
    public void showChangePassStep1Error(Throwable throwable) {
//        Toast.makeText(getContext(), "Помилка сервера", Toast.LENGTH_LONG).show();
    }
}