package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

import rinojermina.app.R;
import rinojermina.app.api.response.User;
import rinojermina.app.presenter.ChangePassStep1Presenter;
import rinojermina.app.presenter.ChangePassStep2Presenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IChangePassStep2View;

public class ChangePassStep2Fragment extends BaseFragment<ChangePassStep2Presenter> implements IChangePassStep2View {
    String token;

    TextInputLayout newPassInputLayout;
    TextInputLayout passUpdApproveInputLayout;

    EditText newPass;
    EditText passConfirm;

    Button updateBtn;
    Button backBtn;

    LinearLayoutCompat layoutBtnBack;

    private ChangePassStep2Presenter presenter;

    public ChangePassStep2Fragment() {
        // Required empty public constructor
    }

    public static ChangePassStep2Fragment newInstance(String param1, String param2) {
        ChangePassStep2Fragment fragment = new ChangePassStep2Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ChangePassStep2Presenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_update_password_update, container, false);

        newPassInputLayout = rootView.findViewById(R.id.newPassInputLayout);
        passUpdApproveInputLayout = rootView.findViewById(R.id.passUpdApproveInputLayout);
        newPass = rootView.findViewById(R.id.newPass);
        passConfirm = rootView.findViewById(R.id.passConfirm);
        updateBtn = rootView.findViewById(R.id.updateBtn);
        backBtn = rootView.findViewById(R.id.backBtn);
        layoutBtnBack = rootView.findViewById(R.id.layoutBtnBack);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPassInputLayout.setErrorEnabled(false);
                passUpdApproveInputLayout.setErrorEnabled(false);

                presenter.step2PassUpd(
                        token = SystemUtil.getToken(getContext()),
//                        token = SystemUtil.setToken(getContext(), );               getToken(getContext()),
//                        token = "plZVF1RdbipPRb9VobGS4in7R8Rf5sClEUdlESEmLf5JeN5G3R",
                        newPass.getText().toString(),
                        passConfirm.getText().toString());

//                Navigation.findNavController(rootView).navigate(R.id
//                        .action_updatePasswordFragmentUpdate2_to_signInFragment2);
            }
        });

        layoutBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(rootView).navigate(R.id
                        .action_updatePasswordFragmentUpdate2_to_updatePasswordFragmentForward2);
            }
        });

        return rootView;
    }

    @Override
    public void newPassError() {
        newPassInputLayout.setErrorEnabled(true);
    }

    @Override
    public void newPassApproveError() {
        passUpdApproveInputLayout.setErrorEnabled(true);
    }

    @Override
    public void showAuth(User user) {
//        if (user.getToken().equals(SystemUtil.getToken(getContext()))) {
            SystemUtil.saveNewPass(getContext(), user);
            Navigation.findNavController(requireView()).navigate(R.id
                    .action_updatePasswordFragmentUpdate2_to_signInFragment2);
//        }
    }

    @Override
    public void showChangePassStep2Error(Throwable throwable) {

    }
}