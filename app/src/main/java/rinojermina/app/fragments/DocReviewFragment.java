package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;

public class DocReviewFragment extends Fragment {

    LinearLayoutCompat question1Layout;
    LinearLayoutCompat question2Layout;
    LinearLayoutCompat question3Layout;
    LinearLayoutCompat question1OpenedLayout;
    LinearLayoutCompat question2OpenedLayout;
    LinearLayoutCompat question3OpenedLayout;

    public DocReviewFragment() {
        // Required empty public constructor
    }

    public static DocReviewFragment newInstance() {
        DocReviewFragment fragment = new DocReviewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_doc_review, container, false);
        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setTitle(getString(R.string.doc_review));
        }

//        url = "https://www.youtube-nocookie.com/embed/WaVsZsyJwA4";
//        video = rootView.findViewById(R.id.video);
////        videoView = rootView.findViewById(R.id.videoView);
////        video.clearCache(true);
////        video.clearHistory();
//        video.getSettings().setJavaScriptEnabled(true);
//        video.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        video.canGoBack();
//        video.canGoForward();
//        video.loadUrl(url);

//        question1Layout = rootView.findViewById(R.id.question1Layout);
//        question2Layout = rootView.findViewById(R.id.question2Layout);
//        question3Layout = rootView.findViewById(R.id.question3Layout);
//        question1OpenedLayout = rootView.findViewById(R.id.question1OpenedLayout);
//        question2OpenedLayout = rootView.findViewById(R.id.question2OpenedLayout);
//        question3OpenedLayout = rootView.findViewById(R.id.question3OpenedLayout);
//
//        question1Layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                question1Layout.setVisibility(View.GONE);
//                question1OpenedLayout.setVisibility(View.VISIBLE);
////                question2Layout.setVisibility(View.GONE);
////                question3Layout.setVisibility(View.GONE);
//            }
//        });
//
//        question1OpenedLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                question1OpenedLayout.setVisibility(View.GONE);
//                question1Layout.setVisibility(View.VISIBLE);
////                question2Layout.setVisibility(View.VISIBLE);
////                question3Layout.setVisibility(View.VISIBLE);
//            }
//        });
//
//        question2Layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                question2Layout.setVisibility(View.GONE);
//                question2OpenedLayout.setVisibility(View.VISIBLE);
////                question3Layout.setVisibility(View.GONE);
//            }
//        });
//
//        question2OpenedLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                question2OpenedLayout.setVisibility(View.GONE);
//                question2Layout.setVisibility(View.VISIBLE);
////                question3Layout.setVisibility(View.VISIBLE);
//            }
//        });
//
//        question3Layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                question3Layout.setVisibility(View.GONE);
//                question3OpenedLayout.setVisibility(View.VISIBLE);
//            }
//        });
//
//        question3OpenedLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                question3OpenedLayout.setVisibility(View.GONE);
//                question3Layout.setVisibility(View.VISIBLE);
//            }
//        });

        return rootView;
    }
}