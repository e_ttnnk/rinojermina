package rinojermina.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.text.TextRecognizer;

import rinojermina.app.R;

public class DrugNameFragment extends Fragment {

    //Переменная для установления связи между приложением и камерой
    private int CAMERA_CAPTURE;
    //Дает возможность обратиться к захваченному камерой изображению
    private Uri pUri;

//    CameraSource cameraSource;

    AppCompatEditText drugName;
    AppCompatImageButton cameraIBtn;

    Button okBtn;

    public DrugNameFragment() {
        // Required empty public constructor
    }

    public static DrugNameFragment newInstance() {
        DrugNameFragment fragment = new DrugNameFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_drug_name, container, false);

        drugName = rootView.findViewById(R.id.drugName);
        cameraIBtn = rootView.findViewById(R.id.cameraIBtn);
        okBtn = rootView.findViewById(R.id.okBtn);

        cameraIBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                cameraOn();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(rootView).navigate(R.id.action_drugNameFragment_to_profileCreateFragment);
            }
        });

        return rootView;
    }

    private void cameraOn(View view) {
        //Используем стандартное системное намерение на использование камеры:
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //Задаем возможность работать с полученными с камеры данными:
        startActivityForResult(captureIntent, CAMERA_CAPTURE);
    }

    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getContext();

        // Create the TextRecognizer
        TextRecognizer textRecognizer = new TextRecognizer.Builder(context).build();
        // Set the TextRecognizer's Processor.

        // Check if the TextRecognizer is operational.
//        if (!textRecognizer.isOperational()) {
//            Log.w(TAG, "Detector dependencies are not yet available.");
//
//            // Check for low storage.  If there is low storage, the native library will not be
//            // downloaded, so detection will not become operational.
//            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
//            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;
//
//            if (hasLowStorage) {
//                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
//                Log.w(TAG, getString(R.string.low_storage_error));
//            }
//        }
        // Create the mCameraSource using the TextRecognizer.
//        cameraSource =
//                new CameraSource.Builder(context.getApplicationContext(), textRecognizer)
//                        .setFacing(CameraSource.CAMERA_FACING_BACK)
//                        .setRequestedPreviewSize(1280, 1024)
//                        .setRequestedFps(15.0f)
////                        .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
////                        .setFocusMode(autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO : null)
//                        .build();
    }
}