package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import rinojermina.app.R;
import rinojermina.app.api.response.Task;
import rinojermina.app.api.response.User;
import rinojermina.app.presenter.MessageSetPresenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IMessageSetView;

//import static rinojermina.app.fragments.MessageSettingsFragment.STAGE;

public class DrugTakingStageFragment extends Fragment {
//    public static final String STAGE = "stage";


    RadioButton firstStageBtn;
    RadioButton secondStageBtn;
    RadioButton thirdStageBtn;

    LinearLayoutCompat layoutFirst;
    LinearLayoutCompat layoutSecond;
    LinearLayoutCompat layoutThird;

    Button nextBtn;

    Integer actualStage;

    public DrugTakingStageFragment() {
        // Required empty public constructor
    }

    public static DrugTakingStageFragment newInstance() {
        DrugTakingStageFragment fragment = new DrugTakingStageFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Integer stage = getIntent().getIntExtra(STAGE, 1);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_drug_taking_stage, container, false);

        firstStageBtn = rootView.findViewById(R.id.firstStageBtn);
        secondStageBtn = rootView.findViewById(R.id.secondStageBtn);
        thirdStageBtn = rootView.findViewById(R.id.thirdStageBtn);

        layoutFirst = rootView.findViewById(R.id.layoutFirst);
        layoutSecond = rootView.findViewById(R.id.layoutSecond);
        layoutThird = rootView.findViewById(R.id.layoutThird);

        nextBtn = rootView.findViewById(R.id.nextBtn);

        layoutFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFalseCheck();
                firstStageBtn.setChecked(true);
                actualStage = 1;
                stageSaved(actualStage);
//                secondStageBtn.setChecked(false);
//                thirdStageBtn.setChecked(false);
            }
        });

        layoutSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFalseCheck();
//                firstStageBtn.setChecked(false);
                secondStageBtn.setChecked(true);
                actualStage = 2;
                stageSaved(actualStage);
//                thirdStageBtn.setChecked(false);

            }
        });

        layoutThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFalseCheck();
//                firstStageBtn.setChecked(false);
//                secondStageBtn.setChecked(false);
                thirdStageBtn.setChecked(true);
                actualStage = 3;
                stageSaved(actualStage);
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageSettingsFragment.newInstance(actualStage);
                Navigation.findNavController(rootView).navigate(R.id.action_drugTakingStageFragment_to_messageSettingsFragment);
            }
        });

        return rootView;
    }

    public void setFalseCheck() {
        firstStageBtn.setChecked(false);
        secondStageBtn.setChecked(false);
        thirdStageBtn.setChecked(false);
    }

    public void stageSaved(Integer actualStage) {
        SystemUtil.setStage(getContext(), actualStage);
    }
}