package rinojermina.app.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import rinojermina.app.R;
import rinojermina.app.activity.MainActivity;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.presenter.SignInPresenter;
import rinojermina.app.utils.SystemUtil;

public class EnteringFragment extends Fragment {

    Button signUpBtn;
    Button loginBtn;

    public EnteringFragment() {
        // Required empty public constructor
    }

    public static EnteringFragment newInstance() {
        EnteringFragment fragment = new EnteringFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_entering, container, false);

        loginBtn = rootView.findViewById(R.id.loginBtn);
        signUpBtn = rootView.findViewById(R.id.signUpBtn);

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(rootView).navigate(R.id.action_enteringFragment_to_drugNameFragment);
            }
        });


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(rootView).navigate(R.id.action_enteringFragment_to_signInFragment);
            }
        });

        return rootView;
    }

}