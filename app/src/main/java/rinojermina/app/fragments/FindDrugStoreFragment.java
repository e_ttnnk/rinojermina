package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;

public class FindDrugStoreFragment extends Fragment {
    private String url;

    WebView drugStoreFind;

    public FindDrugStoreFragment() {
        // Required empty public constructor
    }

    public static FindDrugStoreFragment newInstance() {
        FindDrugStoreFragment fragment = new FindDrugStoreFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_find_drug_store, container, false);
        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setTitle(getString(R.string.find_drugstore));
        }
        url = "https://api.rinogermina.com.ua/api/v3/map.php";
        drugStoreFind = rootView.findViewById(R.id.drugStoreFind);
        drugStoreFind.getSettings().setJavaScriptEnabled(true);
        drugStoreFind.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        drugStoreFind.canGoBack();
        drugStoreFind.canGoForward();
        drugStoreFind.loadUrl(url);

        return rootView;
    }
}