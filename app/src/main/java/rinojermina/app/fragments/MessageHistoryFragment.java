package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Objects;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.adapter.MessageHistoryAdapter;
import rinojermina.app.api.response.Push;
import rinojermina.app.api.response.Status;
import rinojermina.app.presenter.AppointmentCalendarPresenter;
import rinojermina.app.presenter.MessageHistoryPresenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IAppointmentCalendarView;
import rinojermina.app.view.IMessageHistoryView;
import rinojermina.app.view.listener.PushClickListener;

public class MessageHistoryFragment extends BaseFragment<MessageHistoryPresenter>
        implements IMessageHistoryView, PushClickListener {

    RecyclerView messageHistoryRecycler;
    private Toolbar toolbar;

    private MessageHistoryPresenter presenter;


    public MessageHistoryFragment() {
        // Required empty public constructor
    }

    public static MessageHistoryFragment newInstance() {
        MessageHistoryFragment fragment = new MessageHistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MessageHistoryPresenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_message_history, container, false);
        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setTitle(getString(R.string.message_history));
        }
        messageHistoryRecycler = rootView.findViewById(R.id.messageHistoryRecycler);
        presenter.messagesShow(SystemUtil.getToken(getContext()));
        return rootView;
    }

    @Override
    public void showMessages(List<Push> pushes) {
        SystemUtil.read(getContext(), false);
        for (int i = 0; i < pushes.size(); i++) {
            if (pushes.get(i).getReaded().equals("0")) {
                SystemUtil.read(getContext(), true);
            }
        }

//        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
//            ((NavigationDrawerActivity) getActivity()).setMessageIcon();
//        }
        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setBurger();
        }
        messageHistoryRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        messageHistoryRecycler.setAdapter(new MessageHistoryAdapter(getContext(), pushes, this));
    }

    @Override
    public void updPushStatus(Status status) {
        Log.d("Ok!", "All Right!");
    }

    @Override
    public void showError(Throwable error) {
//        SystemUtil.read(getContext(), false);
        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setBurger();
        }
    }

    @Override
    public void onMessageClicked(Push push) {
//        SystemUtil.setPushId(getContext(), push.getPushId());
        presenter.messageStatusChange(SystemUtil.getToken(getContext()), push.getPushId());

//        push.setReaded("1");
//        replaceFragment(OnePushFragment.newInstance());
// TODO: 28.01.2022 тут переход на фрагмент с открытием одного сообщения, в котором уже делаем презентер
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager manager = requireActivity().getSupportFragmentManager();
        if (manager != null) {
            manager.beginTransaction()
                    .replace(rinojermina.app.R.id.container, fragment).addToBackStack(null).commit();
        }
    }
}