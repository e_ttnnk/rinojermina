package rinojermina.app.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;

import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

//import rinojermina.app.api.response.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.api.response.Task;
import rinojermina.app.api.response.User;
import rinojermina.app.presenter.MessageSetPresenter;
import rinojermina.app.presenter.SignInPresenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IMessageSetView;

//import static rinojermina.app.activity.MainActivity.STAGE;
import static rinojermina.app.activity.WebActivity.LINK;
import static rinojermina.app.activity.WebActivity.TITLE;

public class MessageSettingsFragment extends BaseFragment<MessageSetPresenter> implements IMessageSetView {
//    public static final String STAGE = "stage";


    TextView datePicking;
    //    TextView timePicking;
    TextView morningTimePicking;
    TextView eveningTimePicking;

    Button saveBtn;

//    Integer stage;

    Calendar dateAndTime = Calendar.getInstance();

    private MessageSetPresenter presenter;

    public MessageSettingsFragment() {
        // Required empty public constructor
    }

    public static MessageSettingsFragment newInstance(Integer stage) {
        MessageSettingsFragment fragment = new MessageSettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        presenter = new MessageSetPresenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_message_settings, container, false);

        datePicking = rootView.findViewById(R.id.date_picking);
//        timePicking = rootView.findViewById(R.id.time_picking);
        morningTimePicking = rootView.findViewById(R.id.morning_time_picking);
        eveningTimePicking = rootView.findViewById(R.id.evening_time_picking);
        saveBtn = rootView.findViewById(R.id.saveBtn);

        setInitialDate(datePicking);
//        setInitialTime(timePicking, morningTimePicking, eveningTimePicking);
//        setInitialTime(timePicking);
        setInitialMorningTime(morningTimePicking);
        setInitialEveningTime(eveningTimePicking);

        datePicking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate(datePicking);
            }
        });

//        timePicking.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setTime(timePicking);
//            }
//        });

        morningTimePicking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMorningTime(morningTimePicking);
            }
        });

        eveningTimePicking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEveningTime(eveningTimePicking);
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.messageSet(SystemUtil.getToken(getContext()),
                        SystemUtil.getStage(getContext()),
                        datePicking.getText().toString(),
                        morningTimePicking.getText().toString(),
                        eveningTimePicking.getText().toString());

//                Navigation.findNavController(rootView).navigate(R.id.action_messageSettingsFragment_to_messageHistoryFragment);
//                Intent intent = new Intent(getContext(), NavigationDrawerActivity.class);
//                startActivity(intent);
//                replaceFragment(MessageSettingsFragment.newInstance());
            }
        });

        return rootView;
    }

    private void setInitialDate(View v) {
        int year = dateAndTime.get(Calendar.YEAR);
        int month = dateAndTime.get(Calendar.MONTH) + 1;
        int day = dateAndTime.get(Calendar.DAY_OF_MONTH);

        String dateString = String.format("%02d.%02d.%d", day, month, year);
        datePicking.setText(dateString);

    }


    private void setInitialMorningTime(View v) {
        int hour = dateAndTime.get(Calendar.HOUR_OF_DAY);
        int minute = dateAndTime.get(Calendar.MINUTE);

        String timeString = String.format("%02d:%02d", hour, minute);
        morningTimePicking.setText(timeString);
    }

    private void setInitialEveningTime(View v) {
        int hour = dateAndTime.get(Calendar.HOUR_OF_DAY);
        int minute = dateAndTime.get(Calendar.MINUTE);

        String timeString = String.format("%02d:%02d", hour, minute);
        eveningTimePicking.setText(timeString);
    }

    // установка обработчика выбора даты
    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDate(datePicking);
        }
    };

    // отображаем диалоговое окно для выбора даты
    public void setDate(View v) {
        new DatePickerDialog(getContext(), d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    // установка обработчика выбора времени
//    TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {
//        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
//            dateAndTime.set(Calendar.MINUTE, minute);
//            setInitialTime(timePicking);
//            setInitialTime(timePicking, morningTimePicking, eveningTimePicking);
//        }
//    };

    // отображаем диалоговое окно для выбора времени
//    public void setTime(View v) {
//        new TimePickerDialog(getContext(), t,
//                dateAndTime.get(Calendar.HOUR_OF_DAY),
//                dateAndTime.get(Calendar.MINUTE), true)
//                .show();
//    }

    TimePickerDialog.OnTimeSetListener tm = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int nextMinute = 0;

            if (minute >= 45 && minute <= 59)
                nextMinute = 45;
            else if (minute  >= 30)
                nextMinute = 30;
            else if (minute >= 15)
                nextMinute = 15;
            else if (minute > 0)
                nextMinute = 0;
            else {
                nextMinute = 45;
            }

            if (minute - nextMinute == 1) {
                if (minute >= 45 && minute <= 59)
                    nextMinute = 00;
                else if (minute >= 30)
                    nextMinute = 45;
                else if (minute >= 15)
                    nextMinute = 30;
                else if (minute > 0)
                    nextMinute = 15;
                else {
                    nextMinute = 15;
                }
            }
            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
//            dateAndTime.set(Calendar.MINUTE, minute);
            dateAndTime.set(Calendar.MINUTE, nextMinute);
            setInitialMorningTime(morningTimePicking);
        }
    };

    public void setMorningTime(View v) {
        new TimePickerDialog(getContext(), tm,
                dateAndTime.get(Calendar.HOUR_OF_DAY),
                dateAndTime.get(Calendar.MINUTE), true)
                .show();
    }

    TimePickerDialog.OnTimeSetListener te = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int nextMinute = 0;

            if (minute >= 45 && minute <= 59)
                nextMinute = 45;
            else if (minute  >= 30)
                nextMinute = 30;
            else if (minute >= 15)
                nextMinute = 15;
            else if (minute > 0)
                nextMinute = 0;
            else {
                nextMinute = 45;
            }

            if (minute - nextMinute == 1) {
                if (minute >= 45 && minute <= 59)
                    nextMinute = 00;
                else if (minute >= 30)
                    nextMinute = 45;
                else if (minute >= 15)
                    nextMinute = 30;
                else if (minute > 0)
                    nextMinute = 15;
                else {
                    nextMinute = 15;
                }
            }
            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
//            dateAndTime.set(Calendar.MINUTE, minute);
            dateAndTime.set(Calendar.MINUTE, nextMinute);
            setInitialEveningTime(eveningTimePicking);
        }
    };

    public void setEveningTime(View v) {
        new TimePickerDialog(getContext(), te,
                dateAndTime.get(Calendar.HOUR_OF_DAY),
                dateAndTime.get(Calendar.MINUTE), true)
                .show();
    }

    @Override
    public void showMainScreen() {
        Intent intent = new Intent(getContext(), NavigationDrawerActivity.class);
        startActivity(intent);
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(getContext(), R.string.no_user, Toast.LENGTH_SHORT).show();
    }

//    private void replaceFragment(Fragment fragment) {
//        FragmentManager manager = requireActivity().getSupportFragmentManager();
//        if (manager != null) {
//            manager.beginTransaction()
//                    .replace(rinojermina.app.R.id.container, fragment).commit();
//        }
//    }
}