package rinojermina.app.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import rinojermina.app.R;
import rinojermina.app.activity.MainActivity;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.activity.SplashActivity;
import rinojermina.app.adapter.MenuAdapter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.listener.MenuSelectedListener;

public class NavigationFragment extends Fragment implements MenuSelectedListener {

    AppCompatImageView closeBtn;
    RecyclerView recyclerView;
    LinearLayoutCompat addReminderLayout;

    public NavigationFragment() {
        // Required empty public constructor
    }

    public static NavigationFragment newInstance() {
        NavigationFragment fragment = new NavigationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_navigation, container, false);

        closeBtn = rootView.findViewById(R.id.closeButton);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
                    ((NavigationDrawerActivity) getActivity()).close();
                }
            }
        });

        recyclerView = rootView.findViewById(R.id.menuRecycler);
        setupMenu();
        addReminderLayout = rootView.findViewById(R.id.addReminderLayout);
        addReminderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(AddOtherDrug1Fragment.newInstance(getString(R.string.add_drug_title)));
                ((NavigationDrawerActivity) getActivity()).close();
            }
        });
        replaceFragment(MessageHistoryFragment.newInstance());
        return rootView;
    }

    private void setupMenu() {
//        SystemUtil.read(getContext(), false);
        MenuAdapter adapter = new MenuAdapter(getContext(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
//        replaceFragment(MessageHistoryFragment.newInstance());
    }

    @SuppressLint("CommitTransaction")
    @Override
    public void onMenuItemSelected(int position) {
        String token = SystemUtil.getToken(getContext());
        switch (position) {
            case 0:
                replaceFragment(MessageHistoryFragment.newInstance());
                break;
            case 1:
                replaceFragment(AppointmentCalendarFragment.newInstance());
                break;
            case 2:
//                String linkVideo = "https://www.youtube-nocookie.com/embed/WaVsZsyJwA4";
                String linkVideo = "https://api.rinogermina.com.ua/api/v3/video.php";
                replaceFragment(WebFragment.newInstance(linkVideo, getString(R.string.video_play)));

//                replaceFragment(VideoFragment.newInstance());
                break;
            case 3:
                String linkDocRev = "https://api.rinogermina.com.ua/api/v3/anketa?token=" + token;
                replaceFragment(WebFragment.newInstance(linkDocRev, getString(R.string.doc_review)));

//                replaceFragment(DocReviewFragment.newInstance());
                break;
            case 4:
                String linkProf = "https://api.rinogermina.com.ua/api/v3/profile?token=" + token;
                replaceFragment(WebFragment.newInstance(linkProf, getString(R.string.profile)));

//                replaceFragment(ProfileFragment.newInstance());
                break;
            case 5:
                String linkDrug = "https://api.rinogermina.com.ua/api/v3/map.php";
                replaceFragment(WebFragment.newInstance(linkDrug, getString(R.string.find_drugstore)));

//                replaceFragment(FindDrugStoreFragment.newInstance());
                break;
            case 6:
//                SystemUtil.read(getContext(), false);
                SystemUtil.logOut(getContext());
                startActivity(new Intent(getActivity(), SplashActivity.class));
                getActivity().finish();
//                replaceFragment(EnteringFragment.newInstance());
                break;
//            default:
//                replaceFragment(MessageHistoryFragment.newInstance());
//                break;
//                return super.onOptionsItemSelected((MenuItem) MessageHistoryFragment.newInstance());
        }
        ((NavigationDrawerActivity) getActivity()).close();
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager manager = requireActivity().getSupportFragmentManager();
        if (manager != null) {
            manager.beginTransaction()
                    .replace(rinojermina.app.R.id.container, fragment).commit();
        }
    }
}