package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import rinojermina.app.R;
import rinojermina.app.api.response.Push;
import rinojermina.app.presenter.OnePushPresenter;
import rinojermina.app.presenter.ProfileCreatePresenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IOnePushView;
import rinojermina.app.view.IProfileCreateView;

public class OnePushFragment extends BaseFragment<OnePushPresenter> implements IOnePushView {

    LinearLayoutCompat messageLayout;
    AppCompatTextView date;
    AppCompatTextView text;

    private OnePushPresenter presenter;

    public OnePushFragment() {
        // Required empty public constructor
    }

    public static OnePushFragment newInstance() {
        OnePushFragment fragment = new OnePushFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new OnePushPresenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_one_push, container, false);

        date = rootView.findViewById(R.id.date);
        text = rootView.findViewById(R.id.text);

        presenter.onePushShow(SystemUtil.getToken(getContext()), SystemUtil.getPushId(getContext()));

        return rootView;

        // TODO: 28.01.2022 пишем презентер для просмотра одного пуша
    }

    @Override
    public void showOnePush(Push push) {
        date.setText(push.getDate());
        text.setText(push.getText());
//        push.setReaded("1");
        // TODO: 29.01.2022 отобразить сообщение
    }

//    @Override//?????????????
//    public void onResume() {
//        super.onResume();
//
//        if(getView() == null){
//            return;
//        }
//
//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
//                    // handle back button's click listener
//                    return true;
//                }
//                return false;
//            }
//        });
//    }

    @Override
    public void showError(Throwable error) {

    }
}