package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rinojermina.app.R;
import rinojermina.app.api.response.all_reminders.Task;
import rinojermina.app.api.response.all_reminders.Time;
import rinojermina.app.presenter.OneReminderPresenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IOneReminderView;

public class OneReminderFragment extends BaseFragment<OneReminderPresenter> implements IOneReminderView {

    LinearLayoutCompat reminder1Layout;
    LinearLayoutCompat reminder2Layout;
    LinearLayoutCompat reminder3Layout;
    AppCompatTextView reminderName;
    AppCompatTextView reminderTime1;
    AppCompatTextView reminderTime2;
    AppCompatTextView reminderTime3;
    AppCompatButton addDrug;

    private OneReminderPresenter presenter;

    public OneReminderFragment() {
        // Required empty public constructor
    }

    public static OneReminderFragment newInstance() {
        OneReminderFragment fragment = new OneReminderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new OneReminderPresenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_one_reminder, container, false);

        reminder1Layout = rootView.findViewById(R.id.reminder1Layout);
        reminder2Layout = rootView.findViewById(R.id.reminder2Layout);
        reminder3Layout = rootView.findViewById(R.id.reminder3Layout);
        reminderName = rootView.findViewById(R.id.reminderName);
        reminderTime1 = rootView.findViewById(R.id.reminderTime1);
        reminderTime2 = rootView.findViewById(R.id.reminderTime2);
        reminderTime3 = rootView.findViewById(R.id.reminderTime3);
        addDrug = rootView.findViewById(R.id.addDrug);
        addDrug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(AddOtherDrug2Fragment.newInstance());
            }
        });

        presenter.oneReminderShow(SystemUtil.getToken(getContext()), SystemUtil.getTaskId(getContext()));

        return rootView;
    }

    @Override
    public void showOneReminder(Task task) {
        reminderName.setText(task.getName());
        for (int i = 0; i < task.getTimes().size(); i++) {
            Time time = task.getTimes().get(i);
            if (i == 0) {
                reminder1Layout.setVisibility(View.VISIBLE);
                reminderTime1.setText(time.getTime());
            }
            if (i == 1) {
                reminder2Layout.setVisibility(View.VISIBLE);
                reminderTime2.setText(time.getTime());
            }
            if (i == 2) {
                reminder3Layout.setVisibility(View.VISIBLE);
                reminderTime3.setText(time.getTime());
            }
        }
// TODO: 05.02.2022 поотображать время установленное
    }

    @Override
    public void showError(Throwable error) {

    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager manager = requireActivity().getSupportFragmentManager();
        if (manager != null) {
            manager.beginTransaction()
                    .replace(rinojermina.app.R.id.container, fragment).addToBackStack(null).commit();
        }
    }
}