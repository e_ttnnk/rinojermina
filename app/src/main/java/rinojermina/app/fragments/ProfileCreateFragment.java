package rinojermina.app.fragments;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.opengl.Visibility;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.android.gms.vision.text.TextRecognizer;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.api.response.User;
import rinojermina.app.firebase.MyFirebaseMessagingService;
import rinojermina.app.presenter.ProfileCreatePresenter;
import rinojermina.app.presenter.SignInPresenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IProfileCreateView;

public class ProfileCreateFragment extends BaseFragment<ProfileCreatePresenter> implements IProfileCreateView {

    TextInputLayout phoneInputLayout;
    TextInputLayout passwordInputLayout;
    TextInputLayout birthYearInputLayout;
    TextInputLayout docNameInputLayout;
    TextInputLayout docPhoneInputLayout;

    EditText phone;
    EditText password;
    EditText birthYear;
    EditText docName;
    EditText docPhone;

    Button saveBtn;

    CheckBox agreedCB;

    private ProfileCreatePresenter presenter;

    public ProfileCreateFragment() {
        // Required empty public constructor
    }

    public static ProfileCreateFragment newInstance() {
        ProfileCreateFragment fragment = new ProfileCreateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ProfileCreatePresenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_create, container, false);
        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setTitle(getString(R.string.profile));
        }

        phoneInputLayout = rootView.findViewById(R.id.phoneInputLayout);
        passwordInputLayout = rootView.findViewById(R.id.passwordInputLayout);
        birthYearInputLayout = rootView.findViewById(R.id.birthYearInputLayout);
        docNameInputLayout = rootView.findViewById(R.id.docNameInputLayout);
        docPhoneInputLayout = rootView.findViewById(R.id.docPhoneInputLayout);

        phone = rootView.findViewById(R.id.phone);
        password = rootView.findViewById(R.id.password);
        birthYear = rootView.findViewById(R.id.birthYear);
        docName = rootView.findViewById(R.id.docName);
        docPhone = rootView.findViewById(R.id.docPhone);

        agreedCB = rootView.findViewById(R.id.agreedCB);

        saveBtn = rootView.findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneInputLayout.setErrorEnabled(false);
                passwordInputLayout.setErrorEnabled(false);
                birthYearInputLayout.setErrorEnabled(false);
                presenter.profileCreate(
//                        SystemUtil.setToken(getContext(), ),
                        phone.getText().toString(),
                        password.getText().toString(),
                        birthYear.getText().toString(),
                        docName.getText().toString(),
                        docPhone.getText().toString());

//                Navigation.findNavController(rootView).navigate(R.id.action_profileCreateFragment_to_drugTakingStageFragment);
            }
        });

        return rootView;
    }

    @Override
    public void showPhoneNumberError() {
        phoneInputLayout.setErrorEnabled(true);
        phoneInputLayout.setError(getString(R.string.reg_phone_error));
    }

    @Override
    public void showPassError() {
        passwordInputLayout.setErrorEnabled(true);
        passwordInputLayout.setError(getString(R.string.auth_valid_error));
    }

    @Override
    public void birthYearError() {
        birthYearInputLayout.setErrorEnabled(true);
        birthYearInputLayout.setError(getString(R.string.reg_birth_year_error));
    }

    @Override
    public void showDrugStageScreen(User user) {
        String fcm = MyFirebaseMessagingService.getToken(requireContext());
        presenter.fcmUpdate(user.getToken(), fcm);
        SystemUtil.saveNewProfile(getContext(), user);
        if (agreedCB.isChecked()) {
            Navigation.findNavController(requireView()).navigate(R.id.action_profileCreateFragment_to_drugTakingStageFragment);
        }
    }

    @Override
    public void fcmUpd(User user) {
        SystemUtil.saveFcm(getContext(), user.getFcm_token());
    }

    @Override
    public void showRegError(Throwable error) {

    }
}