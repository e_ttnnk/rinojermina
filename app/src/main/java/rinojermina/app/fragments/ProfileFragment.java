package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;

import java.util.Objects;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.utils.SystemUtil;

public class ProfileFragment extends Fragment {

    private String url;

    WebView profile;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setTitle(getString(R.string.profile));
        }
        String token = SystemUtil.getToken(requireActivity());
        profile = rootView.findViewById(R.id.profile);
        url = "https://api.rinogermina.com.ua/api/v3/profile?token=" + token;
        profile.canGoBack();
        profile.canGoForward();
        profile.loadUrl(url);
        return rootView;
    }
}