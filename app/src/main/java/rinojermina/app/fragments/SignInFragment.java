package rinojermina.app.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.api.response.User;
import rinojermina.app.firebase.MyFirebaseMessagingService;
import rinojermina.app.presenter.SignInPresenter;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.ISignInView;

public class SignInFragment extends BaseFragment<SignInPresenter> implements ISignInView {

    TextInputLayout phoneInputLayout;
    TextInputLayout passwordInputLayout;

    EditText phoneNumberEt;
    EditText passwordEt;

    Button loginBtn;
    Button updatePassBtn;

    private SignInPresenter presenter;

    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SignInPresenter(this);
        setPresenter(presenter);
//        JsonFactory.calendarBody("plZVF1RdbipPRb9VobGS4in7R8Rf5sClEUdlESEmLf5JeN5G3R", 1, "19.01.2022", "11:45", "18:00");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);

        phoneInputLayout = rootView.findViewById(R.id.phoneInputLayout);
        passwordInputLayout = rootView.findViewById(R.id.passwordInputLayout);

        phoneNumberEt = rootView.findViewById(R.id.phoneNumberEt);
        passwordEt = rootView.findViewById(R.id.passwordEt);

        loginBtn = rootView.findViewById(R.id.loginBtn);
        updatePassBtn = rootView.findViewById(R.id.updatePassBtn);

        updatePassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(rootView).navigate(R.id.action_signInFragment_to_updatePasswordFragmentForward2);
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneInputLayout.setErrorEnabled(false);
                passwordInputLayout.setErrorEnabled(false);
                presenter.signIn(
                        phoneNumberEt.getText().toString(),
                        passwordEt.getText().toString());

//                Navigation.findNavController(rootView).navigate(R.id.action_signInFragment_to_messageHistoryFragment);
//                Intent intent = new Intent(getContext(), NavigationDrawerActivity.class);
//                startActivity(intent);

            }
        });

        return rootView;
    }

    @Override
    public void showPhoneNumberError() {
        phoneInputLayout.setErrorEnabled(true);
        phoneInputLayout.setError(getString(R.string.auth_valid_error));
    }

    @Override
    public void showPassError() {
        passwordInputLayout.setErrorEnabled(true);
        passwordInputLayout.setError(getString(R.string.auth_valid_error));
    }

    @Override
    public void showMainScreen(User user) {
        String fcm = MyFirebaseMessagingService.getToken(requireContext());
//        Log.d("!@#", fcm);
        presenter.fcmUpdate(user.getToken(), fcm);
        if (getActivity() != null) {
            SystemUtil.saveUser(getActivity(), user);
            SystemUtil.logIn(getContext());
            startActivity(new Intent(getActivity(), NavigationDrawerActivity.class));
            getActivity().finish();
        }
    }

    @Override
    public void fcmUpd(User user) {
        SystemUtil.saveFcm(getContext(), user.getFcm_token());
    }

    @Override
    public void showAuthError(Throwable error) {

    }
}