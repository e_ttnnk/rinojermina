package rinojermina.app.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.MediaController;
import android.widget.VideoView;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;

public class VideoFragment extends Fragment {

    private static final String URL_PARAM = "url";
    private String url;

    WebView video;
//    VideoView videoView;

    public VideoFragment() {
        // Required empty public constructor
    }

    public static VideoFragment newInstance() {
        VideoFragment fragment = new VideoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video, container, false);
        if (getActivity() != null && getActivity() instanceof NavigationDrawerActivity) {
            ((NavigationDrawerActivity) getActivity()).setTitle(getString(R.string.video_play));
        }
        url = "https://www.youtube-nocookie.com/embed/WaVsZsyJwA4";
        video = rootView.findViewById(R.id.video);
//        videoView = rootView.findViewById(R.id.videoView);
//        video.clearCache(true);
//        video.clearHistory();
        video.getSettings().setJavaScriptEnabled(true);
        video.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        video.canGoBack();
        video.canGoForward();
        video.loadUrl(url);




        return rootView;
    }
}