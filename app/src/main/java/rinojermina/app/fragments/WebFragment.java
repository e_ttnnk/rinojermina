package rinojermina.app.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;

import rinojermina.app.R;
import rinojermina.app.activity.NavigationDrawerActivity;
import rinojermina.app.utils.OurViewClient;

import static rinojermina.app.activity.WebActivity.LINK;
import static rinojermina.app.activity.WebActivity.TITLE;

public class WebFragment extends Fragment {

    private String link;
    private String title;
    private WebView webView;

    public WebFragment() {
        // Required empty public constructor
    }

    public static WebFragment newInstance(String link, String title) {
        WebFragment fragment = new WebFragment();
        Bundle args = new Bundle();
        args.putString(LINK, link);
        args.putString(TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.link = getArguments().getString(LINK);
            this.title = getArguments().getString(TITLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_web, container, false);
        webView = rootView.findViewById(R.id.webView);
        setTitle(title);

        if (!TextUtils.isEmpty(link)) {
            loadPage(link);
        }

        webView.setWebViewClient(new OurViewClient(){
            public void onPageFinished(WebView webView, String link) {
                super.onPageFinished(webView, link);

                WindowManager manager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);

                DisplayMetrics metrics = new DisplayMetrics();
                manager.getDefaultDisplay().getMetrics(metrics);

                metrics.widthPixels /= metrics.density;

                loadPage("javascript:var scale = " + metrics.widthPixels + " / document.body.scrollWidth; document.body.style.zoom = scale;");
                CookieSyncManager.getInstance().sync();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        return rootView;
    }

    private void loadPage(String link) {
        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.loadUrl(link);
    }

    public void setTitle(String title){
        if(title != null && getActivity() != null && getActivity() instanceof NavigationDrawerActivity){
            ((NavigationDrawerActivity)getActivity()).setTitle(title);
        }
    }
}