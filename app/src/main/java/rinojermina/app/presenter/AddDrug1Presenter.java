package rinojermina.app.presenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.JsonFactory;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.view.IAddDrug1View;
import rinojermina.app.view.IMessageHistoryView;

public class AddDrug1Presenter extends BasePresenter {
    private String TAG = AddDrug1Presenter.class.getSimpleName();
    private IAddDrug1View drug1View;

    public AddDrug1Presenter(IAddDrug1View view) {
        this.drug1View = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void remindersShow(String token) {
        compositeDisposable.add(RetrofitFactory.getRinoApi().getReminders(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(allRemindersResponse -> {
                    if (!allRemindersResponse.getTask().isEmpty()) {
                        drug1View.showReminders(allRemindersResponse.getTask());
                    } else {
                        drug1View.showError(null);
                    }
                }, throwable -> {
                    drug1View.showError(throwable);
                }));
    }

    public void delOneReminder(String token, String id) {
        compositeDisposable.add(RetrofitFactory.getRinoApi().delOneReminder(JsonFactory.delOneReminderBody(
                token, id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onePushDelResponse -> {
                    if (onePushDelResponse.getStatus().getSuccess()) {
                        drug1View.delReminder(onePushDelResponse.getTask());
                    } else {
                        drug1View.showError(null);
                    }
                }, throwable -> {
                    drug1View.showError(throwable);
                }));
    }

//    public void oneReminderShow(String token, String id) {
//        compositeDisposable.add(RetrofitFactory.getRinoApi().getOneReminder(token, id)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(allRemindersResponse -> {
//                    if (!allRemindersResponse.getTask().isEmpty()) {
//                        drug1View.showOneReminder(allRemindersResponse.getTask());
//                    } else {
//                        drug1View.showError(null);
//                    }
//                }, throwable -> {
//                    drug1View.showError(throwable);
//                }));
//    }
}
