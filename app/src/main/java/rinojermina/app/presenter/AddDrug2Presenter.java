package rinojermina.app.presenter;


import com.google.gson.JsonObject;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.JsonFactory;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.view.IAddDrug2View;

public class AddDrug2Presenter extends BasePresenter {
    public static final String NOT_SET = "";

    private String TAG = AddDrug2Presenter.class.getSimpleName();
    private IAddDrug2View drug2View;

    public String time1 = NOT_SET;
    public String time2 = NOT_SET;
    public String time3 = NOT_SET;

    public String time1Id = NOT_SET;
    public String time2Id = NOT_SET;
    public String time3Id = NOT_SET;



    public AddDrug2Presenter(IAddDrug2View view) {
        this.drug2View = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void reminderCreate(String token, String name) {
        JsonObject body = JsonFactory.createReminderAndroid(token, name, generateTime());
        compositeDisposable.add(RetrofitFactory.getRinoApi().newReminder(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(newReminderResponse -> {
                    if (!newReminderResponse.getTask().getTimes().isEmpty()) {
                        drug2View.reminderCreate();
                    } else {
                        drug2View.showError(null);
                    }
                }, throwable -> {
                    drug2View.showError(throwable);
                }));
    }

    public int timesCount() {
        return generateTime().size();
    }


    private ArrayList<String> generateTime() {
        ArrayList<String> times = new ArrayList<>();

        if (!time1.equals(NOT_SET)) times.add(time1);
        if (!time2.equals(NOT_SET)) times.add(time2);
        if (!time3.equals(NOT_SET)) times.add(time3);

        return times;
    }

    public void delOnePushReminder(String token, String id) {
        if (id == NOT_SET) return;
        compositeDisposable.add(RetrofitFactory.getRinoApi().delOnePush(JsonFactory.delOnePushBody(
                token, id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onePushDelResponse -> {
                    if (onePushDelResponse.getStatus().getSuccess()) {
                        drug2View.delOnePush(onePushDelResponse.getTask());
                    } else {
                        drug2View.showError(null);
                    }
                }, throwable -> {
                    drug2View.showError(throwable);
                }));
    }
}
