package rinojermina.app.presenter;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.JsonFactory;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.api.response.Calendar;
import rinojermina.app.view.IAppointmentCalendarView;

public class AppointmentCalendarPresenter extends BasePresenter {
    private String TAG = AppointmentCalendarPresenter.class.getSimpleName();
    private IAppointmentCalendarView appointmentCalendarView;

    public AppointmentCalendarPresenter(IAppointmentCalendarView view) {
        this.appointmentCalendarView = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void dateRangeShow(String token) {
        compositeDisposable.add(RetrofitFactory.getRinoApi().getCalendar(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(calendarResponse -> {
                    if (!calendarResponse.getCalendar().isEmpty()) {
                        appointmentCalendarView.showDates(calendarResponse.getCalendar());
                    } else {
                        appointmentCalendarView.showError(null);
                    }
                }, throwable -> {
                    appointmentCalendarView.showError(throwable);
                }));
    }

}
