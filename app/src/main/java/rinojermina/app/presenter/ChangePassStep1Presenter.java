package rinojermina.app.presenter;

import android.text.TextUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.JsonFactory;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.view.IChangePassStep1View;

public class ChangePassStep1Presenter extends BasePresenter {
    private String TAG = ChangePassStep1Presenter.class.getSimpleName();
    private IChangePassStep1View step1View;

    public ChangePassStep1Presenter(IChangePassStep1View view) {
        this.step1View = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void step1PassUpd(String phone, String birthYear) {
        String clearNumber = phone.replaceAll("\\s+", "");
        if (TextUtils.isEmpty(clearNumber) || clearNumber.length() != 13) {
            step1View.phoneNumberError();
            return;
        }
        if (TextUtils.isEmpty(birthYear) || birthYear.length() < 4) {
            step1View.birthYearError();
            return;
        }
        compositeDisposable.add(RetrofitFactory.getRinoApi().change_pass_step_1(JsonFactory
                .passUpdStep1Body(clearNumber, birthYear))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(changePassStep1Response -> {
                    if (changePassStep1Response.getStatus().getSuccess()) {
                        step1View.showNextStep(changePassStep1Response.getUser());
                    } else {
                        step1View.showChangePassStep1Error(null);
                    }
                }, throwable -> {
                    step1View.showChangePassStep1Error(throwable);
                }));
    }
}
