package rinojermina.app.presenter;

import android.text.TextUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.JsonFactory;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.view.IChangePassStep1View;
import rinojermina.app.view.IChangePassStep2View;

public class ChangePassStep2Presenter extends BasePresenter {
    private String TAG = ChangePassStep2Presenter.class.getSimpleName();
    private IChangePassStep2View step2View;

    public ChangePassStep2Presenter(IChangePassStep2View view) {
        this.step2View = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void step2PassUpd(String token, String newPass, String passConfirm) {
        if (TextUtils.isEmpty(newPass) || newPass.length() < 4) {
            step2View.newPassError();
            return;
        }
        if (TextUtils.isEmpty(passConfirm) || passConfirm.length() < 4 ||
                !newPass.equals(passConfirm)) {
            step2View.newPassApproveError();
            return;
        }
        compositeDisposable.add(RetrofitFactory.getRinoApi().change_pass_step_2(JsonFactory
                .passUpdStep2Body(token, passConfirm))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(changePassStep2Response -> {
                    if (changePassStep2Response.getStatus().getSuccess()) {
                        step2View.showAuth(changePassStep2Response.getUser());
                    } else {
                        step2View.showChangePassStep2Error(null);
                    }
                }));
    }
}
