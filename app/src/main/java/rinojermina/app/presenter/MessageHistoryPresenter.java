package rinojermina.app.presenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.JsonFactory;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IMessageHistoryView;

public class MessageHistoryPresenter extends BasePresenter {
    private String TAG = MessageHistoryPresenter.class.getSimpleName();
    private IMessageHistoryView messageHistoryView;

    public MessageHistoryPresenter(IMessageHistoryView view) {
        this.messageHistoryView = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void messagesShow(String token) {
        compositeDisposable.add(RetrofitFactory.getRinoApi().getPushHistory(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pushHistoryResponse -> {
                    if (!pushHistoryResponse.getPush().isEmpty()) {
                        messageHistoryView.showMessages(pushHistoryResponse.getPush());
                    } else {
                        messageHistoryView.showError(null);
                    }
                }, throwable -> {
                    messageHistoryView.showError(throwable);
                }));
    }

    public void messageStatusChange(String token, String push_id) {
        compositeDisposable.add(RetrofitFactory.getRinoApi().updatePush(JsonFactory.updatePushStatusBody(token, push_id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pushStatusResponse -> {
                    if (pushStatusResponse.getStatus().getSuccess()) {
                        messageHistoryView.updPushStatus(pushStatusResponse.getStatus());
                    } else {
                        messageHistoryView.showError(null);
                    }
                }, throwable -> {
                    messageHistoryView.showError(throwable);
                }));
    }
}
