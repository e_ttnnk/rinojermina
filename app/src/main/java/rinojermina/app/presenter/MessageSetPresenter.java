package rinojermina.app.presenter;

import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.JsonFactory;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.view.IMessageSetView;

public class MessageSetPresenter extends BasePresenter {
    private String TAG = MessageSetPresenter.class.getSimpleName();
    private IMessageSetView drugStageView;

    public MessageSetPresenter(IMessageSetView view) {
        this.drugStageView = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void messageSet(String token, Integer stage, String dateBegin,
                           String day, String night) {
//        JsonObject jsonObject = JsonFactory.calendarBody(token, stage, dateBegin,
//                day, night);
        compositeDisposable.add(RetrofitFactory.getRinoApi().calendar(JsonFactory.calendarBody(
                token, stage, dateBegin, day, night))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(calendarResponse -> {
                    if (!calendarResponse.getCalendar().isEmpty()) {
                        drugStageView.showMainScreen();
                    } else {
                        drugStageView.showError(null);
                    }
                }, throwable -> {
                    drugStageView.showError(throwable);
                }));
    }
}
