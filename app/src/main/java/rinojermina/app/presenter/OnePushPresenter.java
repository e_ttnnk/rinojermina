package rinojermina.app.presenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.api.response.Push;
import rinojermina.app.view.IOnePushView;

public class OnePushPresenter extends BasePresenter {
    private String TAG = OnePushPresenter.class.getSimpleName();
    private IOnePushView onePushView;

    public OnePushPresenter(IOnePushView view) {
        this.onePushView = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void onePushShow(String token, String pushId) {
        compositeDisposable.add(RetrofitFactory.getRinoApi().getOnePush(token, pushId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((pushHistoryResponse -> {
                    if (!pushHistoryResponse.getPush().isEmpty()) {
                        for (int i = 0; i < pushHistoryResponse.getPush().size(); i++) {
                            Push push = pushHistoryResponse.getPush().get(i);
                            if (push.getPushId().equals(pushId)) {
                                onePushView.showOnePush(push);
                            }
                        }
                    } else {
                        onePushView.showError(null);
                    }
                }), throwable -> {
                    onePushView.showError(throwable);
                }));
    }
}
