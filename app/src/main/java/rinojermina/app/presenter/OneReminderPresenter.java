package rinojermina.app.presenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.api.response.Push;
import rinojermina.app.api.response.all_reminders.Task;
import rinojermina.app.view.IOneReminderView;

public class OneReminderPresenter extends BasePresenter {
    private String TAG = OneReminderPresenter.class.getSimpleName();
    private IOneReminderView oneReminderView;

    public OneReminderPresenter(IOneReminderView view) {
        this.oneReminderView = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void oneReminderShow(String token, String id) {
        compositeDisposable.add(RetrofitFactory.getRinoApi().getOneReminder(token, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((allRemindersResponse -> {
                    if (!allRemindersResponse.getTask().isEmpty()) {
                        for (int i = 0; i < allRemindersResponse.getTask().size(); i++) {
                            Task task = allRemindersResponse.getTask().get(i);
                            if (task.getId().equals(id)) {
                                oneReminderView.showOneReminder(task);
                            }
                        }
                    } else {
                        oneReminderView.showError(null);
                    }
                }), throwable -> {
                    oneReminderView.showError(throwable);
                }));
    }
}