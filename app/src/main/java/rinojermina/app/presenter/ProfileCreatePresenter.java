package rinojermina.app.presenter;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.JsonFactory;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.utils.SystemUtil;
import rinojermina.app.view.IProfileCreateView;

public class ProfileCreatePresenter extends BasePresenter {
    private String TAG = ProfileCreatePresenter.class.getSimpleName();
    private IProfileCreateView regView;

    public ProfileCreatePresenter(IProfileCreateView view) {
        this.regView = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void profileCreate(String phone, String password, String birth_year,
                              String doc_fio, String doc_phone) {
        String clearNumber = phone.replaceAll("\\s+", "");
        if (TextUtils.isEmpty(clearNumber) || clearNumber.length() != 13) {
            regView.showPhoneNumberError();
            return;
        }
        if (TextUtils.isEmpty(password) || password.length() < 4) {
            regView.showPassError();
            return;
        }
        if (TextUtils.isEmpty(birth_year) || birth_year.length() < 4) {
            regView.birthYearError();
            return;
        }
//        JsonObject jsonObject = JsonFactory.regBody(clearNumber,
//                password, birth_year, doc_fio, doc_phone);
        compositeDisposable.add(RetrofitFactory.getRinoApi().registration(JsonFactory.regBody(clearNumber,
                password, birth_year, doc_fio, doc_phone))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(profileCreateResponse -> {
                    if (profileCreateResponse.getStatus().getSuccess()) {
                        Log.d(TAG, "signed up");
                        regView.showDrugStageScreen(profileCreateResponse.getUser());
                    } else {
                        regView.showRegError(null);
                    }
                }, throwable -> {
                    regView.showRegError(throwable);
                }));
    }

    public void fcmUpdate(String token, String fcmToken) {
        compositeDisposable.add(RetrofitFactory.getRinoApi().updateFcmTokenReg(JsonFactory.fcmTokenUpd(token, fcmToken))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        profileCreateResponse -> {
                    if (profileCreateResponse.getStatus().getSuccess()) {
                        regView.fcmUpd(profileCreateResponse.getUser());
                    } else {
                        regView.showRegError(null);
                    }
                }, throwable -> {
                    regView.showRegError(throwable);
                }));
    }
}
