package rinojermina.app.presenter;

import android.text.TextUtils;
import android.util.Log;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rinojermina.app.api.JsonFactory;
import rinojermina.app.api.RetrofitFactory;
import rinojermina.app.view.ISignInView;

import static rinojermina.app.api.response.StringResponse.TRUE;


public class SignInPresenter extends BasePresenter {
    private String TAG = SignInPresenter.class.getSimpleName();
    private ISignInView authView;

    public SignInPresenter(ISignInView view) {
        this.authView = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void signIn(String phone, String password) {
        String clearNumber = phone.replaceAll("\\s+", "");
        if (TextUtils.isEmpty(clearNumber) || clearNumber.length() != 13) {
            authView.showPhoneNumberError();
            return;
        }
        if (TextUtils.isEmpty(password) || password.length() < 4) {
            authView.showPassError();
            return;
        }
        compositeDisposable.add(RetrofitFactory.getRinoApi().auth(JsonFactory.loginBody(clearNumber, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(loginResponse -> {
                    if (loginResponse.getStatus().getSuccess()) {
                        Log.d(TAG, "signed up");
                        authView.showMainScreen(loginResponse.getUser());
                    } else {
                        Log.d(TAG, "sign up error");
                        authView.showAuthError(null);
                    }
                }, throwable -> {
                    Log.d(TAG, "sign up error");
                    authView.showAuthError(throwable);
                }));
    }

    public void fcmUpdate(String token, String fcmToken) {
        compositeDisposable.add(RetrofitFactory.getRinoApi().updateFcmTokenAuth(JsonFactory.fcmTokenUpd(token, fcmToken))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(loginResponse -> {
                    if (loginResponse.getStatus().getSuccess()) {
                        authView.fcmUpd(loginResponse.getUser());
                    } else {
                        authView.showAuthError(null);
                    }
                }, throwable -> {
                    authView.showAuthError(throwable);
                }));
    }
}
