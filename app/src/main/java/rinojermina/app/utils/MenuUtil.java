package rinojermina.app.utils;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import rinojermina.app.R;
import rinojermina.app.model.MenuItem;

public class MenuUtil {
//    private static List<MenuItem> list;

    public static List<MenuItem> generateMenu(Context context) {
        List<MenuItem>
        list = new ArrayList<>();
//        SystemUtil.read(context, false);
        // TODO: 08.02.2022 разобраться с правильным отображением(авториз полный +380503825605 111111 пустой +380555555555 11111
        if (SystemUtil.isReaded(context)) {
            list.add(new MenuItem(R.drawable.ic_message_history_pink_point, context.getString(R.string.message_history)));
        } else {
            list.add(new MenuItem(R.drawable.ic_message_history, context.getString(R.string.message_history)));
        }
//        list.add(new MenuItem(R.drawable.ic_message_history, context.getString(R.string.message_history)));
        list.add(new MenuItem(R.drawable.ic_calendar_edit, context.getString(R.string.appointment_calendar)));
        list.add(new MenuItem(R.drawable.ic_video_play, context.getString(R.string.video_play)));
        list.add(new MenuItem(R.drawable.ic_doc_review, context.getString(R.string.doc_review)));
        list.add(new MenuItem(R.drawable.ic_profile, context.getString(R.string.profile)));
        list.add(new MenuItem(R.drawable.ic_find_drugstore, context.getString(R.string.find_drugstore)));
        list.add(new MenuItem(R.drawable.ic_logout, context.getString(R.string.logout)));
        return list;
    }

//    public void setMessageIcon(Context context) {
//        if (SystemUtil.isReaded(context)) {
//            list.add(new MenuItem(R.drawable.ic_message_history_pink_point, context.getString(R.string.message_history)));
//        } else {
//            list.add(new MenuItem(R.drawable.ic_message_history, context.getString(R.string.message_history)));
//        }
//    }
}
