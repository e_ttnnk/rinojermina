package rinojermina.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import rinojermina.app.api.response.User;

import static rinojermina.app.api.JsonFactory.*;

public class SystemUtil {
    private static final String IS_AUTH = "is_auth";
    private static final String IS_READED = "is_readed";

    public static void read(Context context, boolean isReaded) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(IS_READED, isReaded).apply();
    }

    public static boolean isReaded(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(IS_READED, false);
    }

    public static void logOut(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        read(context, false);
        preferences.edit().putBoolean(IS_AUTH, false).apply();
    }

    public static void logIn(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(IS_AUTH, true).apply();
    }

    public static boolean isLogin(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(IS_AUTH, false);
    }

    public static void saveUser(Context context, User user) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN, user.getToken());
        editor.putString(PASSWORD, user.getPassword());
        editor.putString(PHONE, user.getPhone());
        editor.apply();
    }

    public static void saveFcm(Context context, String fcm_token) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(FCM_TOKEN, fcm_token).apply();
    }

    public static String getFcm(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(FCM_TOKEN, "");
    }

    public static void saveNewProfile(Context context, User user) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN, user.getToken());
        editor.putString(PHONE, user.getPhone());
        editor.putString(PASSWORD, user.getPassword());
        editor.putString(BIRTH_YEAR, user.getBirth_year());
        editor.putString(DOC_FIO, user.getDoc_fio());
        editor.putString(DOC_PHONE, user.getDoc_phone());
        editor.apply();
    }

    public static String getPassToken(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PASSWORD, "");
    }

    public static void saveNewPass(Context context, User user) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN, user.getToken());
        editor.putString(PASSWORD, user.getPassword());
        editor.apply();
    }

    public static String getPhone(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PHONE, "");
    }

    public static String getBirthYear(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(BIRTH_YEAR, "");
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(TOKEN, "");
    }

    public static void setToken(Context context, String token) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(TOKEN, token).apply();
    }

    public static void setStage(Context context, Integer stage) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putInt(String.valueOf(STAGE), stage).apply();
    }

    public static int getStage(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(String.valueOf(STAGE), 1);
    }

    public static void setPushId(Context context, String push_id) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(PUSH_ID, push_id).apply();
    }

    public static String getPushId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PUSH_ID, "");
    }

    public static void setTaskId(Context context, String id) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(TASK_ID, id).apply();
    }

    public static String getTaskId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(TASK_ID, "");
    }


//    public static void saveCalendarList(Context context, Calendar calendar) {
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString(DATE_BEGIN, calendar.getDateBegin());
//        editor.putString(DATE_END, calendar.getDateEnd());
//        editor.apply();
//    }
}
