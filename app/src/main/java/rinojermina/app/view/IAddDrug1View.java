package rinojermina.app.view;

import java.util.List;

import rinojermina.app.api.response.all_reminders.Task;

public interface IAddDrug1View {
    void showReminders(List<Task> tasks);
    void delReminder(rinojermina.app.api.response.one_push_del.Task task);
    void showOneReminder(List<Task> tasks);
    void showError(Throwable error);
}
