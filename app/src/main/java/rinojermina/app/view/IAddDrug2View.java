package rinojermina.app.view;

public interface IAddDrug2View {
    void reminderCreate();
    void delOnePush(rinojermina.app.api.response.one_push_del.Task task);
    void showError(Throwable error);
}
