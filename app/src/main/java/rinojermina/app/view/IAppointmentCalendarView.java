package rinojermina.app.view;

import java.util.ArrayList;
import java.util.Calendar;

public interface IAppointmentCalendarView {
    void showDates(ArrayList<rinojermina.app.api.response.Calendar> calendars);
    void showError(Throwable error);

}
