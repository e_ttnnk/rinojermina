package rinojermina.app.view;

import rinojermina.app.api.response.User;

public interface IChangePassStep1View {
    void phoneNumberError();
    void birthYearError();
    void showNextStep(User user);
    void showChangePassStep1Error(Throwable throwable);
}
