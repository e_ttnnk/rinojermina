package rinojermina.app.view;

import rinojermina.app.api.response.User;

public interface IChangePassStep2View {
    void newPassError();
    void newPassApproveError();
    void showAuth(User user);
    void showChangePassStep2Error(Throwable throwable);
}
