package rinojermina.app.view;

import java.util.ArrayList;
import java.util.List;

import rinojermina.app.api.response.Push;
import rinojermina.app.api.response.Status;


public interface IMessageHistoryView {
    void showMessages(List<Push> pushes);
    void updPushStatus(Status status);
    void showError(Throwable error);
}
