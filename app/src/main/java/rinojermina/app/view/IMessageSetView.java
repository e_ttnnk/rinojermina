package rinojermina.app.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rinojermina.app.api.response.Task;
import rinojermina.app.api.response.User;

public interface IMessageSetView {
//    void showMainScreen(List<Calendar> calendar);
//    void showMainScreen(ArrayList<rinojermina.app.api.response.Calendar> calendar);
    void showMainScreen();
    void showError(Throwable throwable);
}
