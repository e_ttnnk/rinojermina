package rinojermina.app.view;

import java.util.List;

import rinojermina.app.api.response.Push;

public interface IOnePushView {
    void showOnePush(Push push);
    void showError(Throwable error);
}
