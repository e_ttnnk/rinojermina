package rinojermina.app.view;

import rinojermina.app.api.response.all_reminders.Task;

public interface IOneReminderView {
    void showOneReminder(Task task);
    void showError(Throwable error);
}
