package rinojermina.app.view;

import android.view.View;

import rinojermina.app.api.response.User;

public interface IProfileCreateView {
    void showPhoneNumberError();
    void showPassError();
    void birthYearError();
    void showDrugStageScreen(User user);
    void fcmUpd(User user);
    void showRegError(Throwable throwable);
}
