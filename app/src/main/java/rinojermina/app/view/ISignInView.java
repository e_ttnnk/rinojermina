package rinojermina.app.view;

import rinojermina.app.api.response.User;

public interface ISignInView {
    void showPhoneNumberError();
    void showPassError();
    void showMainScreen(User user);
    void fcmUpd(User user);
    void showAuthError(Throwable error);
}
