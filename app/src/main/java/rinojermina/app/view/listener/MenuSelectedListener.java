package rinojermina.app.view.listener;

public interface MenuSelectedListener {
    void onMenuItemSelected(int position);
}
