package rinojermina.app.view.listener;

import rinojermina.app.api.response.Push;

public interface PushClickListener {
    void onMessageClicked(Push push);
}
