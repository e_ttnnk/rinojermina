package rinojermina.app.view.listener;

import rinojermina.app.api.response.all_reminders.Task;

public interface TaskClickListener {
    void onTaskClicked(Task task);
}
