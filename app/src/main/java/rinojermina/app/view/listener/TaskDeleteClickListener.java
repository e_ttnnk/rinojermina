package rinojermina.app.view.listener;

import rinojermina.app.api.response.all_reminders.Task;

public interface TaskDeleteClickListener {
    void onDeleteTaskClicked(Task task);
    void onShowTaskClicked(Task task);
}
